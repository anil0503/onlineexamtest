@extends('layouts.user')

@section('content')

<div class="container-fluid">

   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <div class="page-title-right">

            </div>
            <h3 class="page-title">Participate in the discussion and let your voice be heard! </h3>
         </div>
      </div>
   </div>
   <!-- end page title -->
   <div class="row">
      <div class="col-md-8">
         <div class="card-box1">



            <div class="col-xl-11"><br><br>
               <div style="margin-top:-10px; margin-bottom:-10px; ">
                  <div style="padding:0px 0px 10px 20px; " class="row">
                     <div class="col-1">

                            <div id="panellike" data-id="{{ $post->id }}">
                                    <br>
                                      <span class="like-btn">
                                            {{-- <i id="like{{$post->id}}" style="font-size:20px;" style = "cursor:pointer" class="mdi mdi-triangle {{ auth()->user()->hasLiked($post) ? 'like-post' : '' }}"></i> --}}
                                            <h4><div style="padding-left: 5px" id="like{{$post->id}}-bs3">{{ $post->likers()->get()->count() }}</div></h4>
                                      </span>

                                  </div>
                     </div>
                     <div class="col-11">
                        <div class="row">
                            <div class="col-xl-10">
                               <h4>
                                  <a class="text-body " href="{{ route('userposts.show', $post->id) }}">{{$post->title}} </a>
                               </h4>
                            </div>
                            <!-- end col -->
                            @if(isset($user))
                            @if(Auth::user()->id == $post->user_id)
                                 <div style="margin-top: 5px;" class="col-2">
                                    <a style=" cursor:pointer; color:#20DDC6; font-size: 19px"  data-toggle="modal" data-target="#postedit{{ $post->id }}"><i class="mdi mdi-square-edit-outline"></i></a>
                                    <a  style="cursor:pointer;  color:#f0363b; font-size: 19px" data-toggle="modal" data-target="#postdelete{{ $post->id }}"><i class="mdi mdi-delete-sweep-outline"></i></a>
                                </div>
                                @endif

                                @endif
                            <!-- end col -->
                         </div>                        <div class="row">
                           <div style="padding-left:10px;">
                              <div class="fif fa-hover">
                                 <div class="circle fif"> </div>
                                 <div style="color:#443c8f; padding-left:6px; color:443c8f;" class="fif">
                                    {{ $post->status }}</div>
                              </div>

                           </div>
                           <div style="padding-left:10px;">
                              <div class=" fif fa-hover"><a href="#/comment">
                                    <div class="circle fif"> </div>
                                 </a>
                              </div>
                              <div class="fif"> @foreach($brands as $brand) {{$brand->name}} @endforeach </div>
                           </div>
                           <div style="padding-left:10px;" class=" fif fa-hover"><a href="#/comment"><i style="color:#443c8f" ;="" class="fif fa fa-clock"></i> </a>                      </div>
                           <div class="fif">   {{$post->created_at->diffForHumans()}} </div>

                        </div>

                        <p style="padding-top:5px;">{{ $post->body }}</p>


                        <h5>Total Comments {{$post->commentcount}} </h5>
                        <form method="post" action="{{ route('comment.add') }}">
                           @csrf

                           <div class="row">

                           <div class="col 12">
                              <input type="text" name="comment_body" class="form-control form-control-sm" placeholder="Add Comment"/>
                              <input type="hidden" name="post_id" value="{{ $post->id }}" />
                           </div>

                           <div class="col 3">
                              <input type="submit" class="btn btn-warning btn-xs waves-effect waves-light" value="Add Comment" />
                           </div>
                        </div>
                        </form>
                        <hr />
                        @include('comment._comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])
                     </div>

                  </div>
               </div>
            </div>

            <br>
         </div>
      </div>

      <div class="col-md-3">
         <div class="card-box">
            <h4 align="center" class="header-title mb-3">Voters</h4>

            <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 491px;">
               <div class="inbox-widget slimscroll"
                  style="max-height: 310px; overflow: hidden; width: auto; height: 491px;">
                  <div class="inbox-item">
                     <div class="inbox-item-img"><img src="{{ URL::asset('images/users/user-2.jpg')}}"
                           class="rounded-circle" alt=""></div>
                     <p class="inbox-item-author">Tomaslau</p>
                     <p class="inbox-item-text">I've finished it! See you so...</p>
                     <p class="inbox-item-date">
                        <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                     </p>
                  </div>
                  <div class="inbox-item">
                     <div class="inbox-item-img"><img src="{{ URL::asset('images/users/user-3.jpg')}}"
                           class="rounded-circle" alt=""></div>
                     <p class="inbox-item-author">Stillnotdavid</p>
                     <p class="inbox-item-text">This theme is awesome!</p>
                     <p class="inbox-item-date">
                        <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                     </p>
                  </div>
                  <div class="inbox-item">
                     <div class="inbox-item-img"><img src="{{ URL::asset('images/users/user-4.jpg')}}"
                           class="rounded-circle" alt=""></div>
                     <p class="inbox-item-author">Kurafire</p>
                     <p class="inbox-item-text">Nice to meet you</p>
                     <p class="inbox-item-date">
                        <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                     </p>
                  </div>

                  <div class="inbox-item">
                     <div class="inbox-item-img"><img src="{{ URL::asset('images/users/user-5.jpg')}}"
                           class="rounded-circle" alt=""></div>
                     <p class="inbox-item-author">Shahedk</p>
                     <p class="inbox-item-text">Hey! there I'm available...</p>
                     <p class="inbox-item-date">
                        <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                     </p>
                  </div>
                  <div class="inbox-item">
                     <div class="inbox-item-img"><img src="{{ URL::asset('images/users/user-6.jpg')}}"
                           class="rounded-circle" alt=""></div>
                     <p class="inbox-item-author">Adhamdannaway</p>
                     <p class="inbox-item-text">This theme is awesome!</p>
                     <p class="inbox-item-date">
                        <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                     </p>
                  </div>

                  <div class="inbox-item">
                     <div class="inbox-item-img"><img src="{{ URL::asset('images/users/user-3.jpg')}}"
                           class="rounded-circle" alt=""></div>
                     <p class="inbox-item-author">Stillnotdavid</p>
                     <p class="inbox-item-text">This theme is awesome!</p>
                     <p class="inbox-item-date">
                        <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                     </p>
                  </div>
                  <div class="inbox-item">
                     <div class="inbox-item-img"><img src="{{ URL::asset('images/users/user-4.jpg')}}"
                           class="rounded-circle" alt=""></div>
                     <p class="inbox-item-author">Kurafire</p>
                     <p class="inbox-item-text">Nice to meet you</p>
                     <p class="inbox-item-date">
                        <a href="javascript:(0);" class="btn btn-sm btn-link text-info font-13"> Reply </a>
                     </p>
                  </div>
               </div>
               <div class="slimScrollBar"
                  style="background: rgb(158, 165, 171); width: 8px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 221.94px;">
               </div>
               <div class="slimScrollRail"
                  style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;">
               </div>
            </div> <!-- end inbox-widget -->

         </div>
      </div>

      <div id="postedit{{$post->id}}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
           <div class="modal-content">
              <div class="modal-header tect-center">
                 <h4 class="modal-title " id="full-width-modalLabel">Post <b>{{$post->title }}</b> Edit</h4>
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              {!! Form::open(['action'=>['PostController@update',$post->id],'method' => 'POST'])!!}

            <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
               <label for="title" class="col-md-4 col-form-label">{{ __('Post Title') }}</label>
               </div>
            {!! Form::text('title', $post->title, ['class' => 'form-control']) !!}

            <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
                   <label for="body" class="col-md-4 col-form-label">{{ __('Post Body') }}</label>
               </div>
            {!! Form::text('body', $post->body, ['class' => 'form-control']) !!}

             <div class="form-row">
                                            <div class="form-group col-md-6">
        <div style="text-align:left; margin-left:-12px;  font-size:14px;">
               <label for="pstatus" class="col-md-6 col-form-label ">{{ __('Post Status') }}</label>
               </div>
            <select class="form-control input-lg" name="status" id="pstatus" placeholder="Post Title" required>
           <option >{!!$post->status!!}</option>
            <option value = "Planned">Planned</option>
           <option value = "In Progress">In Progress</option>
           <option value = "Completed">Completed</option>
         </select>
                                                    </div>
                                            <div class="form-group col-md-6">
                                                        <div style="text-align:left; margin-left:-12px;  font-size:14px;">

                   <label for="priority" class="col-md-6 col-form-label">{{ __('Post Priority') }}</label>
                   </div>
        <select class="form-control input-lg" name="priority" id="priority" placeholder="Post Title" required>
          <option >{!!$post->priority!!}</option>

          @foreach($status as $board)
           <option value = "{!!$board->pri_status1!!}">{!!$board->pri_status1!!}</option>
           <option value = "{!!$board->pri_status2!!}">{!!$board->pri_status2!!}</option>
           <option value = "{!!$board->pri_status3!!}">{!!$board->pri_status3!!}</option>
           @endforeach

         </select>                                                  </div>
                                        </div>

                                        <div class="modal-footer">
                                            {{ Form::hidden('_method','PUT')}}
                                            {{Form::submit('Close',['class'=>'btn btn-secondary waves-effect','data-dismiss'=>'modal'])}}
                                            {!! Form::submit('Update Details', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
                                            {!! Form::close() !!}
                                            {{ csrf_field() }}
                                            {{--
                                            {{Form::submit('Save Changes',['class'=>'btn btn-primary waves-effect waves-light'])}} --}}
                                         </div>


                 {{ csrf_field() }}
                 {{--
                 {{Form::submit('Save Changes',['class'=>'btn btn-primary waves-effect waves-light'])}} --}}
              </div>
           </div>
           <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
     </div>


   </div>

</div>
<div id="postdelete{{ $post->id }}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
       <div class="modal-content">
          <div class="modal-header">
             <h4 class="modal-title" id="myCenterModalLabel">Delete <b>{{ $post->title }}</b></h4>
             <button type="button" class="close" data-dismiss="modald" aria-hidden="true">×</button>
          </div>
          <div align="center" class="modal-body">
             <div  class="col-sm-6 col-md-4 col-lg-3">
                <i  style="font-size:100px; color:brown" class="fe-alert-triangle"></i>
             </div>
             <br>
             <div align="center"  class="col-sm-12">
                {!! Form::open(['action' => ['PostController@destroy', $post->id], 'method' => 'POST']) !!}
                <h4>Do you want to delete {{$post->title }} </h4>
                <br>
                <div class="button-list">
                   {{ Form::hidden('_method','DELETE')}}
                   {{Form::submit('Cancel',['class'=>'btn btn-info width-lg waves-effect waves-light','data-dismiss'=>'modal'])}}
                   {{Form::submit('Delete',['class'=>'btn btn-danger width-lg waves-effect waves-light'])}}
                </div>
                {!! Form::close() !!}
             </div>
          </div>
          <!-- /.modal-content -->
       </div>
       <!-- /.modal-dialog -->
    </div>
 </div>
<!-- Large modal -->
<button class="btn btn-primary" data-toggle="modal" data-target="#LoginForm">
        Login modal</button>
    <div class="modal fade" id="LoginForm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h4 class="modal-title" id="myModalLabel">
                        Login/Registration - <a href="http://www.jquery2dotnet.com">jquery2dotnet.com</a></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-bordered">
                                <li class="nav-item active"><a href="#Login"  class="nav-link active" data-toggle="tab">Login</a></li>
                                <li class="nav-item"><a href="#Registration" class="nav-link"  data-toggle="tab">Registration</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="Login">
                                    <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">
                                            Email</label>
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control" id="email1" placeholder="Email" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                            Password</label>
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Email" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                Submit</button>
                                            <a href="javascript:;">Forgot your password?</a>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="Registration">
                                    <form role="form" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">
                                            Name</label>
                                        <div class="col-sm-12">
                                            <div class="row">

                                                    <div class="col-sm-12">
                                                    <input type="text" class="form-control" placeholder="Name" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">
                                            Email</label>
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control" id="email" placeholder="Email" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password" class="col-sm-2 control-label">
                                            Password</label>
                                        <div class="col-sm-12">
                                            <input type="password" class="form-control" id="password" placeholder="Password" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-primary btn-sm">
                                                Save & Continue</button>
                                            <button type="button" class="btn btn-default btn-sm">
                                                Cancel</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div id="OR" class="hidden-xs">
                                OR</div>
                        </div>
                        <div class="col-md-5">
                            <div class="row text-center sign-with">
                                <div class="col-md-12">
                                    <h3>
                                        Sign in with</h3>
                                </div>
                                <div class="col-md-12">
                                    <div class="btn-group btn-group-justified">
                                        <a href="#" class="btn btn-primary">Facebook</a> <a href="#" class="btn btn-danger">
                                            Google</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">

$('#LoginForm').modal('show');
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('i.mdi-triangle, i.glyphicon-thumbs-down').click(function(){
            var id = $(this).parents("#panellike").data('id');
            var c = $('#'+this.id+'-bs3').html();
            var cObjId = this.id;
            var cObj = $(this);
            $.ajax({
               type:'POST',
               url:"{{ route('ajaxRequest')}}",
               data:{
                "_token": "{{ csrf_token() }}",
                   id:id
               },
               success:function(data){
                  if(jQuery.isEmptyObject(data.success.attached)){
                    $('#'+cObjId+'-bs3').html(parseInt(c)-1);
                    $(cObj).removeClass("like-post");
                  }else{
                    $('#'+cObjId+'-bs3').html(parseInt(c)+1);
                    $(cObj).addClass("like-post");
                  }
               }
            });
        });
        $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    });
</script>
<style>
        .like-post{
      color: #443c8f !important;
    }
    </style>
@endsection
