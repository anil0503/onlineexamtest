                {!! Form::model($post, [
                    'method' => 'PATCH',
                    'route' => ['step3.update', $post->id]
                ]) !!}
                <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
                   <label for="title" class="col-md-4 col-form-label">{{ __('Post Title') }}</label>
                   </div>
                {!! Form::text('title', null, ['class' => 'form-control']) !!}

                <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
                       <label for="body" class="col-md-4 col-form-label">{{ __('Post Body') }}</label>
                   </div>
                {!! Form::text('body', null, ['class' => 'form-control']) !!}

                 <div class="form-row">
                                                <div class="form-group col-md-6">
            <div style="text-align:left; margin-left:-12px;  font-size:14px;">
                   <label for="pstatus" class="col-md-6 col-form-label ">{{ __('Post Status') }}</label>
                   </div>
                <select class="form-control input-lg" name="status" id="pstatus" placeholder="Post Title" required>
               <option >{!!$post->status!!}</option>
                <option value = "Planned">Planned</option>
               <option value = "In Progress">In Progress</option>
               <option value = "Completed">Completed</option>
             </select>
                                                        </div>
                                                <div class="form-group col-md-6">
                                                            <div style="text-align:left; margin-left:-12px;  font-size:14px;">

                       <label for="priority" class="col-md-6 col-form-label">{{ __('Post Priority') }}</label>
                       </div>

                       <select class="form-control input-lg" name="priority" id="priority" placeholder="Post Title" required>
              <option >{!!$post->priority!!} </option>


              @foreach($boards as $board)
              @if($post->board_id == $board->id)
               <option value = "{!!$board->pri_status1!!}">{!!$board->pri_status1!!}</option>
               <option value = "{!!$board->pri_status2!!}">{!!$board->pri_status2!!}</option>
               <option value = "{!!$board->pri_status3!!}">{!!$board->pri_status3!!}</option>
               @endif
               @endforeach



             </select>                                                  </div>
                                            </div>



