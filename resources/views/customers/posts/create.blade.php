
@extends('layouts.user')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Post</div>
                <div class="card-body">
                    <form method="post" action="{{ route('posts.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            @csrf
                            <label class="label">Post Title: </label>
                            <input type="text" name="title" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label class="label">Post Body: </label>
                            <textarea name="body" rows="10" cols="30" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection























{{-- @extends('layouts.user')
@section('css')
        <!-- Animation css -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />

        <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<link href="{{ URL::asset('css/verification.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.typekit.net/zyh0use.css">


<script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js')}}"></script>

<script type="text/javascript" src="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>


<script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js')}}"></script>


<!-- MultiStep Form -->
<div class="row">
     <div class="col-md-2">
     </div>
    <div class="col-md-8">

        <div id="msform">
            <!-- progressbar -->
            <ul id="progressbar">

                <li class="active1">Create Brand</li>
                <li >Create Board</li>
                <li >Create Post</li>
                <li class="active4">Send Invites</li>
            </ul>
            <!-- fieldsets -->
        <fieldset>
            <h2 class="fs-title">Create Post</h2>
                <h3 class="fs-subtitle">Create your First Post</h3>
                   {!! Form::open(array('action' => 'PostController@store', 'method' => 'post')) !!}

                   <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
                   <label for="title" class="col-md-4 col-form-label">{{ __('Post Title') }}</label>
                   </div>
                   <input class="form-control input-lg" type="text" name="title" id="title" placeholder="Post Title" required/>
                   <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
                       <label for="body" class="col-md-4 col-form-label">{{ __('Post Body') }}</label>
                   </div>
                   <input class="form-control input-lg" type="textarea" name="body" id="body" placeholder="Post Body" required/>
                   <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
                   </div>
                   <div style="text-align:left; margin-left:-12px; margin-top:-10px; font-size:14px;">
                   </div>
                                <div class="form-row">
                                                <div class="form-group col-md-6">
            <div style="text-align:left; margin-left:-12px;  font-size:14px;">
                   <label for="pstatus" class="col-md-4 col-form-label ">{{ __('Post Status') }}</label>
                   </div>
                <select class="form-control input-lg" name="pstatus" id="pstatus" required>
               <option>Select Priority</option>
               <option value = "Planned">Planned</option>
               <option value = "In Progress">In Progress</option>
               <option value = "Completed">Completed</option>

             </select>                                                </div>
                                                <div class="form-group col-md-6">
                                                            <div style="text-align:left; margin-left:-12px;  font-size:14px;">

                       <label for="priority" class="col-md-4 col-form-label">{{ __('Post Priority') }}</label>
                       </div>
            <select class="form-control input-lg" name="priority" id="priority"  required>
                <option >Select Status</option>
               <option value = "High">High</option>
               <option value = "Modarate">Modarate</option>
               <option value = "Low">Low</option>
             </select>                                                  </div>
                                            </div>




           {{Form::submit('Next',['class'=>'action-button'])}}
        </fieldset>
    </div>

    </div>
    <div class="col-md-2">
     </div>
</div>



<script type="text/javascript" src="{{ URL::asset('js/verification.js')}}"></script> 
<style>
body{
    background:#f7f7f7;
}
#progressbar li.active1:before, #progressbar li.active:after {
    background: #443c8f;
    color: white;
}
#progressbar li.active2:before, #progressbar li.active:after {
    background: #443c8f;
    color: white;
}
#progressbar li.active3:before, #progressbar li.active:after {
    background: #443c8f;
    color: white;
}

</style>
@endsection --}}