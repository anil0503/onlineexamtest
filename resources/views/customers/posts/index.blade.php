@extends('layouts.user')
@section('css')
        <!-- Animation css -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">

                </div>

<h4 class="page-title">Let us know where we can improve.                                        </h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-8">
                <div class="card-box1">

                        <ul class="nav nav-tabs nav-bordered nav-justified">
                            <li class="nav-item">
                                <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link  tab-back active">
                                    All
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#home-b2" data-toggle="tab" aria-expanded="true" class="nav-link tab-back">
                                    Planned
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    In Progress
                                </a>
                            </li>
                            <li class="nav-item">
                                    <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
                                            Completed
                                    </a>
                                </li>
                                <li class="nav-item">
<a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
Expired
</a>
                                    </li>
                        </ul>
                        <br>
                        <div   class="row tab-items">
                        <div  style="padding-bottom:10px;"  class="col-xl-3">
                                <div class="dropdown bootstrap-select show-tick">
                                <select class="selectpicker" multiple="" data-selected-text-format="count" data-style="btn-light" tabindex="-98">
      @foreach ($boards as $board)
        {{ $board->id }}
           @if($loop->index == 0)
            <option selected> {{$board->name}}</option>
           @else
        <option> {{$board->name}}</option>
           @endif

        @endforeach
        </select>
                                    </div>

                        </div>
                         <div style="padding-bottom:10px;"  class="col-xl-6">
                                <div class="input-group clockpicker">
<input style="border:2px red;background-color:#eeeeee" type="text" class="form-control" id="search" placeholder="Search Here">
<div class="input-group-append">
<span style="border:2px red; background-color:#eeeeee" class="input-group-text"><i class="fe-search"></i></span>
</div>
                                </div>
                        </div>
                        <div  class="col-xl-3">
                                <div class="dropdown bootstrap-select show-tick"><select class="selectpicker" multiple="" data-selected-text-format="count > 3" data-style="btn-light" tabindex="-98">
<option>Mustard</option>
<option>Ketchup</option>
<option>Relish</option>
<option>Onions</option>
                                    </select>

                                </div>
                        </div>

                        <div class="col-xl-3">

                        </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" id="home-b2">




@foreach($posts as $post)
<div style="margin-top:-10px; margin-bottom:-10px; border-bottom: 1px solid #dddddd;">
                                    <div style="padding:0px 0px 10px 20px; " class="row">
<div class="col-1">
<p style=" align:center; padding-left:4px; padding-top:10px;">

        {!! Form::open(['url' => 'votes', 'class' => 'votes']) !!}
        <div class="upvote topic" data-post="{{ $post->id }}">
          <a class="upvote vote" data-value="1"><div class="triangle-up">

            </div></a>
          <span class="count">0</span>
          <a class="downvote vote" data-value="-1"><div class="triangle-up">

            </div></a>
        </div>
        {!! Form::close() !!}

</p>
</div>

<div class="col-10">

<div>

<div class="row">
        <div class="col-xl-10">
                <h4>
                <a class="text-body " href="{{ route('posts.show', $post->id) }}">{{$post->title}} </a>
                </h4>
        </div> <!-- end col -->

        <div style="margin-top: 5px;" class="col-2">

                <a style=" cursor:pointer; color:#20DDC6; font-size: 19px"  data-toggle="modal" data-target="#postedit{{ $post->id }}"><i class="mdi mdi-square-edit-outline"></i></a>
                <a  style="cursor:pointer;  color:#f0363b; font-size: 19px" data-toggle="modal" data-target="#postdelete{{ $post->id }}"><i class="mdi mdi-delete-sweep-outline"></i></a>

{{--
               <a data-toggle="modal" data-target=".bs-example-modal-center"></a>
                <a   data-toggle="modal" data-target="postdelete{{ $post->id }}"></a> --}}

        </div> <!-- end col -->
    </div>


</h4>

    <div  class="row">
<div style="padding-left:10px;" >
<div  class="fif fa-hover"><div class="circle fif"> </div>
<div style="color:#443c8f; padding-left:6px; color:443c8f;" class="fif">{{$post->status}}</div></div>


</div>
&ensp;  &emsp;
<div>
<div class=" fif fa-hover"><a href="#/comment"><div class="circle fif"> </div> </a>
</div>


</div>

</div>

<p  class="comment more" style="padding-top:5px;">{{$post->body}}</p>


<div class="fif fa-hover"><a href="#/comment"><i style="color:#20DDC6" ;="" class="fif fa fa-comment"></i> </a>  </div>
<div class="fif"> 15 Comments </div>
<div style="padding-left:10px;" class=" fif fa-hover"><a href="#/comment"><i style="color:#443c8f" ;="" class="fif fa fa-adjust"></i> </a>                      </div>
<div class="fif"> 75% Priyorty </div>


</div>
</div>
<div class="col-1">
</div>
</div>
</div>
<br>


<div class="col-2">
<div id="postedit{{$post->id}}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header tect-center">
                    <h4 class="modal-title " id="full-width-modalLabel">Post <b>{{$post->title }}</b> Edit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                {!! Form::open(['action'=>['PostController@update',$post->id],'method' => 'POST'])!!}
                <div class="modal-body">
                @include('posts.edit')
                </div>

                <div class="modal-footer">
                        {{ Form::hidden('_method','PUT')}}
                        {{Form::submit('Close',['class'=>'btn btn-secondary waves-effect','data-dismiss'=>'modal'])}}
                        {!! Form::submit('Update Details', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
                        {!! Form::close() !!}
                        {{ csrf_field() }}
                        {{--
                        {{Form::submit('Save Changes',['class'=>'btn btn-primary waves-effect waves-light'])}} --}}
                            </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<div id="postdelete{{ $post->id }}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">Delete <b>{{ $post->title }}</b></h4>
                    <button type="button" class="close" data-dismiss="modald" aria-hidden="true">×</button>
                </div>
                <div align="center" class="modal-body">
                    <div  class="col-sm-6 col-md-4 col-lg-3">
                        <i  style="font-size:100px; color:brown" class="fe-alert-triangle"></i>

                    </div><br>
                    <div align="center"  class="col-sm-12">
                          {!! Form::open(['action' => ['PostController@destroy', $post->id], 'method' => 'POST']) !!}
                          <h4>Do you want to delete {{$post->title }} </h4><br>
                                <div class="button-list">
                          {{ Form::hidden('_method','DELETE')}}
                         {{Form::submit('Cancel',['class'=>'btn btn-info width-lg waves-effect waves-light','data-dismiss'=>'modal'])}}
                          {{Form::submit('Delete',['class'=>'btn btn-danger width-lg waves-effect waves-light'])}}

                                </div>
                    {!! Form::close() !!}
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
@endforeach




 <div class="tab-items row">

<div class="col-sm-9">
        <div style="padding-top:10px;" class="dataTables_info" id="row-callback-datatable_info" role="status" aria-live="polite">
                Showing {{ $posts->firstItem() }} to {{ $posts->lastItem() }}
                of total {{$posts->total()}} entries            </div>
</div>
<div class="col-sm-3">
    <div class="dataTables_paginate paging_simple_numbers" id="row-callback-datatable_paginate">

        <ul class="pagination pagination-rounded">

            {!! $posts->links() !!}

        </ul>
    </div>
</div>
                                                        </div>
                                                </div>
                                                <div class="tab-pane" id="profile-b2">
                                                    <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                                    <p class="mb-0">Vakal text here dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                                                </div>
                                                <div class="tab-pane" id="messages-b2">
                                                    <p>Vakal text here dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                                                    <p class="mb-0">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                                </div>
                                            </div>
                                        </div>
                            </div> <!-- end col -->
                            <div  style="margin-left:-50px;" class="col-md-1">
                            </div>
                            <div class="col-md-3">

                                    <div class="resp-container1">
                                            <h4  align="center"  style="padding-top:13px; padding-bottom:13px;" align="center">Create a Request</h4>

                                            <form method="post" action="{{ route('posts.store') }}">
                                                    {{ csrf_field() }}
                                                    @foreach ($brands as $brand)

                                                    <input type="hidden" class="form-control input-lg dynamic" id='Brand' name='Brand' value="{{ $brand->id }}">

                                                                    @endforeach
                                                  <select class="form-control input-lg dynamic" id='Board' name='Board'>
                                                            <option>Select Board</option>
                                                                    @foreach ($boards as $board)
                                                                    <option value="{{ $board->id }}">{{ $board->name }}</option>
                                                                    @endforeach
                                                            </select>


                                                    <textarea style=" font-size:12px; margin-top:10px; padding-bottom:20px;"  name="title" id="message" required="required" class="form-control" data-parsley-trigger="keyup" data-parsley-minlength="15" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" placeholder="Add Tittle and Short Description"></textarea>
                                                    <textarea style=" font-size:12px;  margin-top:10px; padding-bottom:50px;" id="message" required="required" class="form-control" name="body" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" placeholder="Add Additional Details"></textarea>
                                                    <select class="form-control input-lg dynamic" style="margin-top:10px; " id='priority' name='priority'>
                                                            <option>Priority</option>
                                                                    <option value="High">Need it ASAP!</option>
                                                                    <option value="Modarate">Importent but not urgent</option>
                                                                    <option value="Low">Good to Have</option>
                                                            </select>
                                                    <div class="form-group">
                <input type="submit" class="buttonform" />
                                                    </div>
                                                </form>


                                           <br>

                                          </div>




                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
<div id="postcreate" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                                <div  class="modal-dialog modal-dialog-centered">
                                    <div  class="modal-content">

                                        <div class="modal-body">

                                                <div class="row">
                                                        <div class="col-12">
<div class="text-center">
    <h2 class="mt-0">
        <i class="mdi mdi-check"></i>

    </h2>
    <h3 class="mt-0">Your vote has been recorded</h3>

    <p class="w-75 mb-2 mx-auto">How importent is this to you?<br>
    This will help us priortise better</p>

<div class="col-12">


      <button  type="button" class="buttonform1" data-toggle="modal" data-target=".bs-example-modal-center">Submit</button>

      </div>
</div>
</div>
                                                        </div>
                                                        <!-- end col -->
                                                    </div> </form>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->


                    </div> <!-- container -->





                    </div> <!-- end card body-->
       @endsection

                    @section('script')

                            <!-- Magnific Popup-->
                            <script src="{{ URL::asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
                            <script src="{{ URL::asset('/js/tribevote.js')}}"></script>
                            <script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
                            <!-- Tour init js-->
                            <script src="{{ URL::asset('assets/js/pages/lightbox.init.js')}}"></script>










{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <h1>Manage Posts</h1>
            <a href="{{ route('posts.create') }}" class="btn btn-success" style="float: right">Create Post</a>
            <table class="table table-bordered">
                <thead>
                    <th width="80px">Id</th>
                    <th>Title</th>
                    <th width="150px">Action</th>
                </thead>
                <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->title }}</td>
                    <td>
                        <a href="{{ route('posts.show', $post->id) }}" class="btn btn-primary">View Post</a>
                    </td>
                </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div> --}}
<script>
    $(document).ready(function() {
            $('.topic').upvote();

            var upvoted = $('.topic').upvote('upvoted');
            var downvoted = $('.topic').upvote('downvoted');

            $('.vote').on('click', function (e) {
                e.preventDefault();
                var data = {value: $(this).data('value'), post_id: $(this).parent().data('post')};

                if (upvoted === true || downvoted === true) {
                    console.log('already voted');
                } else {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('[name="_token"]').val()
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: '/votes',
                        dataType: 'JSON',
                        data: data
                    });
                }
            });
        });
        </script>

@endsection


