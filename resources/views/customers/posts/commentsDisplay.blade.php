@foreach($comments as $comment)
<div class="display-comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
<div class="inbox-widget" >
   <div style="margin-top:-20px;" class="inbox-item">
      <div class="inbox-item-img"><img src="http://localhost/mail/public/assets/images/users/user-2.jpg" height="50px" class="rounded-circle" alt=""></div>
      <div class="row">
         <div>
            <p  class="inbox-item-author">{{ $comment->user->name }}</p>
         </div>
         @if(Auth::user()->id == $post->user_id)
         <div style="margin-top: -5px;" class="col-2">
                <a style=" cursor:pointer; color:#20DDC6; font-size: 19px"  data-toggle="modal" data-target="#Commentedit{{ $comment->id }}"><i class="mdi mdi-square-edit-outline"></i></a>
                <a  style="cursor:pointer;  color:#f0363b; font-size: 19px" data-toggle="modal" data-target="#Commentdelete{{ $comment->id }}"><i class="mdi mdi-delete-sweep-outline"></i></a>
             </div>
                    </div>
        @endif
         
         <!-- end col -->
      </div>
      <p class="inbox-item-text">{{ $comment->body }}</p>
      <div   class="row">
         <div class="col-md-4">
            <p class="text-muted">{{ $comment->created_at->diffForHumans() }}</p>
         </div>
      </div>
   </div>
</div>
<a href="" id="reply"></a>
<form method="post" action="{{ route('comments.store') }}">
   @csrf
   <div class="form-group">
      <input type="hidden" name="post_id" value="{{ $post_id }}" />
      <input type="hidden" name="parent_id" value="{{ $comment->id }}" />
   </div>
   <div style="margin-top:-35px;"  class="row">
      <div class="col-10">
         <input style="height: calc(1.0em + 0.9rem + 2px); border:2px red;background-color:#eeeeee" type="text" name="body" class="form-control"  class="form-control" placeholder="Leave A Comment">
      </div>
      <div class="col-2">
         <input type="submit" class="btn btn-warning" value="Reply" />
      </div>
   </div>
   <div class="form-group">
   </div>
</form>
</div>
<div id="Commentedit{{$comment->id}}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
         <div class="modal-header tect-center">
            <h4 class="modal-title " id="full-width-modalLabel">Comment of<b>{{$post->title }}</b> Edit</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         </div>
         {!! Form::open(['action'=>['CommentController@update',$comment->id],'method' => 'POST'])!!}
         <div class="modal-body">
            {!! Form::text('body', $comment->body, ['class' => 'form-control']) !!}
         </div>
         <div class="modal-footer">
            {{ Form::hidden('_method','PUT')}}
            {{Form::submit('Close',['class'=>'btn btn-secondary waves-effect','data-dismiss'=>'modal'])}}
            {!! Form::submit('Update Details', ['class' => 'btn btn-primary waves-effect waves-light']) !!}
            {!! Form::close() !!}

            {{--
            {{Form::submit('Save Changes',['class'=>'btn btn-primary waves-effect waves-light'])}} --}}
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>
<div id="Commentdelete{{ $comment->id }}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myCenterModalLabel">Delete <b>{{ $comment->title }}</b></h4>
            <button type="button" class="close" data-dismiss="modald" aria-hidden="true">×</button>
         </div>
         <div align="center" class="modal-body">
            <div  class="col-sm-6 col-md-4 col-lg-3">
               <i  style="font-size:100px; color:brown" class="fe-alert-triangle"></i>
            </div>
            <br>
            <div align="center"  class="col-sm-12">
               {!! Form::open(['action' => ['CommentController@destroy', $comment->id], 'method' => 'POST']) !!}
               <h4>Do you want to delete {{$comment->body }} </h4>
               <br>
               <div class="button-list">
                  {{ Form::hidden('_method','DELETE')}}
                  {{Form::submit('Cancel',['class'=>'btn btn-info width-lg waves-effect waves-light','data-dismiss'=>'modal'])}}
                  {{Form::submit('Delete',['class'=>'btn btn-danger width-lg waves-effect waves-light'])}}
               </div>
               {!! Form::close() !!}
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
</div>
@endforeach
