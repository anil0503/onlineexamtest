<form role="form" method="post" action="{{ route('settings.update',$users->id) }}">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <div class="box-body">

      <div class="form-group">
        <label for="exampleInputEmail1">User Old Password</label>
        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Enter old password" name="oldpassword" >
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">User New Password</label>
        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Enter new password" name="newpassword">
      </div>

      <div class="form-group">
        <label for="exampleInputEmail1">Password Confirmation</label>
        <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Enter new password" name="password_confirmation">
      </div>

 </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Update Password</button>
    </div>
</form>
