@extends('layouts.user')
@section('css')
        <!-- Animation css -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />

        <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')<br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
            <div style="margin-top: 20px;" class="card">
                <div style="padding-top: 20px;" class="text-center w-75 m-auto">
                    <a href="{{ URL::asset('/')}}">
                        <span><img src="{{ URL::asset('images/logo-sm.png')}}" alt="" height="70"></span>
                    </a>
                    <p  class="text-muted mb-4 mt-3">Enter your email address to Reset Password.</p>
                </div>
                <h5 style="margin-top: -20px;"  class="card-header text-center"><b>Reset Password</b></h5>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf


                        <div class="form-group mb-3">
                            <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div >
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <div>
                                <button type="submit" class="btn btn-danger btn-block">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="row mt-3">
                <div class="col-12 text-center">
                    <p class="text-muted">Already have account?  <a href="{{ URL::asset('login')}}" class="text-muted ml-1"><b class="font-weight-semibold">Sign In</b></a></p>
                </div> <!-- end col -->
            </div>
        </div>

    </div>
</div>
@endsection
