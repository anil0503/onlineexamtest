@extends('layouts.master-without-nav')
@section('css')
        <!-- Animation css -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />

        <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')<br><br>
<div class="container">
        <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card">

                        <div style="margin-top: -20px;" class="card-body p-4">

                            <div class="text-center w-75 m-auto">
                                <a href="{{ URL::asset('/')}}">
                                <br>
                                    {{-- <span><img src="{{ URL::asset('/images/logo.png')}}" alt="" height="25"></span> --}}
                                </a>
                                <p  class="text-muted mb-4 mt-3">Enter your email address and password to access admin panel.</p>
                            </div>

                            <h5 style="margin-top: -20px;"  class="auth-title">Sign In</h5>

                            <form method="POST" action="{{ route('login') }}">
                                    @csrf


                                    @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                @if (session('warning'))
                                    <div class="alert alert-warning">
                                        {{ session('warning') }}
                                    </div>
                                @endif
                                    <div class="form-group mb-3">
                                        <label for="email" >{{ __('E-Mail Address') }}</label>

                                        <div>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter your email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group mb-3">
                                        <label for="password" >{{ __('Password') }}</label>

                                        <div>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter your Password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox checkbox-info">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>                                        </div>
                                    </div>


                                    <div>
                                        <div>


                                            <button type="submit" class="btn btn-danger btn-block">
                                                {{ __('Login') }}
                                            </button>
                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <div class="text-center">
                                    <h5 class="mt-3 text-muted">Sign in with</h5>
                                    <ul class="social-list list-inline mt-3 mb-0">

                                        <li class="list-inline-item">
                                            <a href="{{ url('/login/google/redirect') }}" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{ url('/login/facebook/redirect') }}" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                                        </li>
                                        {{-- <a href="{{ url('/redirect') }}" class="btn btn-primary">Login With Google</a> --}}

                                    </ul>
                                </div>


                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->
                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p class="text-muted">Don't have account?  <a href="{{ URL::asset('user/register')}}" class="text-muted ml-1"><b class="font-weight-semibold">Sign Up</b></a></p>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end container -->
                </div>
                <!-- end page -->

                <footer class="footer footer-alt">
                    {{date('Y')}} &copy; Xeria theme by <a href="www.OnlineExam.com" class="text-muted">OnlineExam</a>
                </footer>


</div>
@endsection



