@extends('layouts.vertical.master')
@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                                <div class="col-12">
                                   <div class="page-title-box">
                                      <div class="page-title-right">
                                            <a class="btn btn-success" href="{{ route('questions.create') }}"> Create Question</a>

                                      </div>
                                      <h4 class="page-title">Questions Managment</h4>

                            </div>
                                </div>
                             </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                @if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif

                                        <table id="basic-datatable" class="table dt-responsive nowrap">
                                                <thead><tr>
                                                        <th>No</th>
                                                        <th width="20px">Question</th>
                                                        <th>+ve Marks</th>
                                                        <th>-ve Marks</th>
                                                        <th>Subject</th>
                                                        <th>category</th>
                                                        <th>D'Level</th>
                                                        <th>Action</th>
                                                    </tr>  </thead>

                                                    <tbody>
                                                            @foreach ($questions as $question)
                                                            <tr>
                                                        <td>{{ $question->id }}</td>
                                                        <td>{!! $question->question !!}</td>
                                                        <td>{!! $question->marks !!}</td>
                                                        <td>{!! $question->negative !!}</td>
                                                        <td>{!! $question->subject !!}</td>
                                                        <td>{!! $question->category !!}</td>
                                                        <td>{!! $question->level !!}</td>
                                                        <td>
                                                            <form action="{{ route('questions.destroy',$question->id) }}" method="POST">

                                                                {{-- <a class="btn btn-info" href="{{ route('questions.show',$question->id) }}">Show</a> --}}

                                                                <a class="btn btn-primary" href="{{ route('questions.edit',$question->id) }}">Edit</a>

                                                                @csrf
                                                                @method('DELETE')

                                                                <button type="submit" class="btn btn-danger">Delete</button>
                                                            </form>
                                                        </td>
                                                    </tr> @endforeach</tbody>




                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->




                    </div> <!-- container -->
@endsection

@section('script')

        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection
