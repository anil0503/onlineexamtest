@extends('layouts.vertical.master')
@section('css')
<!-- Jquery Toast css -->
<link href="{{ URL::asset('assets/libs/jquery-toast/jquery-toast.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">
   <br>
   <!-- start page title -->
   <div style="margin-left:-40px; margin-right:-40px; margin-top:-20px" class="row ">
      <div  class="col-12">
         <div class="card">
            <div style="box-shadow: 0 1px 10px rgba(0,0,0,0.19), 0 2px 4px rgba(0,0,0,0.23); ">
               <h2 align="center">Your Account Settings </h2>
               <ul style="padding-left:30px;" class="nav text nav-tabs nav-bordered">
                  <li align="center" class="nav-item">
                     <a href="#home-b1" data-toggle="tab" aria-expanded="false" class="nav-link active">
                        <h5> Account Info </h5>
                     </a>
                  </li>
                  <li align="center"  class="nav-item">
                     <a href="#profile-b1" data-toggle="tab" aria-expanded="true" class="nav-link">
                        <h5> My Plan </h5>
                     </a>
                  </li>
                  <li align="center"  class="nav-item">
                     <a href="#messages-b1" data-toggle="tab" aria-expanded="false" class="nav-link">
                        <h5> Billing & Invoice </h5>
                     </a>
                  </li>
               </ul>
            </div>
            @foreach($brands as $brand)
         </div>
         <div>
            <div  style="padding-left:40px; padding-right:40px;" class="tab-content">
               <div class="tab-pane show active" id="home-b1">

                    <div class="card-box">
                            <ul class="nav nav-pills navtab-bg">
                                <li class="nav-item">
                                    <a href="#userdetails" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                        User Details
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#branddetails" data-toggle="tab" aria-expanded="true" class="nav-link">
                                        Brand Details
                                    </a>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane show active" id="userdetails">
                                        <form method="post" action="{{route('account.updateuser',  Auth::user()->id)}}">
                                                {{ csrf_field() }}
                                                {{ method_field('patch') }}

                                                <input type="text" name="name"  value="{{ Auth::user()->name }}" />

                                                <input type="email" name="email"  value="{{ Auth::user()->email }}" />

                                                <input type="password" name="password" />

                                                <input type="password" name="password_confirmation" />

                                                <button type="submit">Send</button>
                                            </form>
                                    {{-- {!! Form::open(['action'=>['ProfileController@updateuser',Auth::user()->id],'method' => 'PUT','files'=>true])!!}

                                        <div class="row">


                                             <div class="col-lg-8">
                                                <div class="form-group row">
                                                   <label for="name" class="col-3 col-form-label">Your Name</label>
                                                   <div class="col-9">
                                                      <input type="text" required="" parsley-type="name" class="form-control" id="name" value="{{Auth::user()->name}}">
                                                   </div>
                                                </div>

                                                <div class="form-group row">
                                                   <label for="email" class="col-3 col-form-label">E-Mail</label>
                                                   <div class="col-9">
                                                      <input type="email" required="" name="email" class="form-control" id="email" value="{{Auth::user()->email}}">
                                                   </div>
                                                </div>
                                                <div class="form-group row">
                                                   <label for="password" class="col-3 col-form-label">Change Password</label>
                                                   <div class="col-9">
                                                      <input type="password" required="" name="password" class="form-control" id="password" value="{{$brand->password}}">
                                                   </div>
                                                </div>


                                             </div>
                                             <div class="col-lg-4">
                                                <div class="form-group row">
                                                   <div class="text-center w-75 m-auto">
                                                      {!! Form::label("avatar_original","Profile Picture",["class"=>"col-form-label"]) !!}<br>
                                                      <img align="center" id="previewlogo" width="100" height="100" class="rounded-circle img-thumbnail"
                                                         src="{{asset((isset($brand) && $brand->brand!='')?'uploads/'.$brand->brand:'images/noname.jpg')}}"
                                                         height="200px" width="300px"/>
                                                      {!! Form::file("avatar_original",["class"=>"form-control","style"=>"display:none"]) !!}
                                                      <br/>
                                                      <a href="javascript:changeLogo();">Change</a> |
                                                      <a style="color: red" href="javascript:removeLogo()">Remove</a>
                                                      <input type="hidden" style="display: none" value="0" name="removelogo" id="removelogo">
                                                   </div>


                                                </div>
                                             </div>

                                        </div>
                                          <div class="row">
                                             <div class="col-xl-2">
                                                <button type="button" class="btn btn-block  btn-secondary waves-effect waves-light">Deactivate Account</button>
                                             </div>
                                             <div class="col-xl-6">
                                             </div>
                                             <div style="padding-top:10px;" class="col-xl-2">
                                                <button type="submit" class="btn btn-block btn-lg btn-purple waves-effect waves-light">Save Changes</button>
                                                <br> <br>
                                             </div>
                                             <div class="col-2">
                                             </div>
                                          </div>

                                    {!! Form::close() !!} --}}
                                </div>
                                <div class="tab-pane" id="branddetails">


                                        <div class="row">
                                                {!! Form::open(['action'=>['ProfileController@updateuser',$brand->id],'method' => 'POST'])!!}

                                             <div class="col-lg-8">

                                                <div class="form-group row">
                                                   <label for="name" class="col-3 col-form-label">Company  Name</label>
                                                   <div class="col-9">
                                                      <input type="text" required="" name="name" class="form-control" id="name" value="{{$brand->name}}">
                                                   </div>
                                                </div>
                                                <div class="form-group row">
                                                   <label for="email" class="col-3 col-form-label">Company E-Mail</label>
                                                   <div class="col-9">
                                                      <input type="email" required="" name="email" class="form-control" id="email" value="{{Auth::user()->email}}">
                                                   </div>
                                                </div>
                                                <div class="form-group row">
                                                   <label for="subdomain" class="col-3 col-form-label">Subdomain</label>
                                                   <div class="col-9">
                                                      <input type="text" required="" name="email" class="form-control" id="subdomain" value="{{$brand->subdomain}}" readonly>
                                                   </div>
                                                </div>


                                             </div>
                                             <div class="col-lg-4">
                                                <div class="form-group row">
                                                   <div class="text-center w-75 m-auto">
                                                      {!! Form::label("logo","Brand Logo",["class"=>"col-form-label"]) !!}<br>
                                                      <img align="center" id="previewlogo" width="100" height="100" class="rounded-circle img-thumbnail"
                                                         src="{{asset((isset($brand) && $brand->brand!='')?'uploads/'.$brand->brand:'images/noname.jpg')}}"
                                                         height="200px" width="300px"/>
                                                      {!! Form::file("logo",["class"=>"form-control","style"=>"display:none"]) !!}
                                                      <br/>
                                                      <a href="javascript:changeLogo();">Change</a> |
                                                      <a style="color: red" href="javascript:removeLogo()">Remove</a>
                                                      <input type="hidden" style="display: none" value="0" name="removelogo" id="removelogo">
                                                   </div>
                                                   <div class="text-center w-75 m-auto">
                                                      {!! Form::label("fevicon","Brand Fevicon",["class"=>"col-form-label"]) !!}<br>
                                                      <img align="center" id="previewfevicon" width="50" height="50" class="rounded-circle img-thumbnail"
                                                         src="{{asset((isset($brand) && $brand->brand!='')?'uploads/'.$brand->brand:'images/noname.jpg')}}"
                                                         height="50" width="50"/>
                                                      {!! Form::file("fevicon",["class"=>"form-control","style"=>"display:none"]) !!}
                                                      <br/>
                                                      <a href="javascript:changeFevicon();">Change</a> |
                                                      <a style="color: red" href="javascript:removeFevicon()">Remove</a>
                                                      <input type="hidden" style="display: none" value="0" name="removefevicon" id="removefevicon">
                                                   </div>

                                                </div>
                                             </div>
                                             @endforeach
                                          </div>
                                          <div class="row">
                                             <div class="col-xl-2">
                                                <button type="button" class="btn btn-block  btn-secondary waves-effect waves-light">Deactivate Account</button>
                                             </div>
                                             <div class="col-xl-6">
                                             </div>
                                             <div style="padding-top:10px;" class="col-xl-2">
                                                <button type="submit" class="btn btn-block btn-lg btn-purple waves-effect waves-light">Save Changes</button>
                                                <br> <br>
                                             </div>
                                             <div class="col-2">
                                             </div>
                                          </div>

                                </div>

                            </div>
                        </div>





                  <div class="col-3">
                  </div>
                  <div class="col-6">
                  </div>
                  <div style="margin-top:-10px;"  class="col-3">
                  </div>
               </div>
               <div class="tab-pane " id="profile-b1">
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="card-box">
                           <div class="row">
                              <div class="col-12">
                                 <div class="text-center">
                                    <h4 style="font-size:15px;" class="text-dark my-1">Current Plan: </h4>
                                    <h4 style="font-weight:600;">FRMT Yearly</h4>
                                 </div>
                              </div>
                           </div>
                           <div align="center" class="mt-3">
                              <h6 >Your subscription will be automatically renew in 106 days and you will be charged USE $198.00.</h6>
                           </div>
                        </div>
                        <div class="card-box">
                           <div class="row">
                              <div class="col-12">
                                 <div class="text-center">
                                    <h4 style="font-size:15px;" class="text-dark my-1">Payment Details </h4>
                                    <h4 style="font-weight:600;">MasterCard</h4>
                                 </div>
                              </div>
                           </div>
                           <div align="center" class="mt-3">
                              <h4 style="font-size:15px;" class="text-dark my-1"><span style="color:#20DDC6;">Update Payment Details </span></h4>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-8">
                        <div class="card">
                           <div class="card-body">
                              <div class="table-responsive">
                                 <table class="table table-centered mb-0" id="inline-editable">
                                    <thead>
                                       <tr  align="center" style="font-weight:600; color:#414d5f; font-size:16px;">
                                          <td  >Transaction Id</td>
                                          <td>Start Date</td>
                                          <td>Status</td>
                                          <td>Frequency</td>
                                          <td>Status</td>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr align="center" style="color:#111111; font-size:15px;">
                                          <td>A2433L5 </td>
                                          <td> 24-11-2018 </td>
                                          <td> Active </td>
                                          <td> Annual   </td>
                                          <td> <i style="font-size:20px; color:#20DDC6" class="fas fa-file-invoice-dollar"></i></td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                              <!-- end .table-responsive-->
                           </div>
                           <!-- end card-body -->
                        </div>
                     </div>
                  </div>
               </div>
               <div class="tab-pane" id="messages-b1">
                  <div class="row">
                     <div class="col-12">
                        <div class="card-box">
                           <!-- Logo & title -->
                           <div class="clearfix">
                              <div class="float-left">
                                 <img src="assets/images/logo-dark.png" alt="" height="20">
                              </div>
                              <div class="float-right">
                                 <h4 class="m-0 d-print-none">Invoice</h4>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="mt-3">
                                    <p><b>Hello, Stanley Jones</b></p>
                                    <p class="text-muted">Thanks a lot because you keep purchasing our products. Our company
                                       promises to provide high quality products for you as well as outstanding
                                       customer service for every transaction.
                                    </p>
                                 </div>
                              </div>
                              <!-- end col -->
                              <div class="col-md-4 offset-md-2">
                                 <div class="mt-3 float-right">
                                    <p class="m-b-10"><strong>Order Date : </strong> <span class="float-right"> &nbsp;&nbsp;&nbsp;&nbsp; Jan 17, 2019</span></p>
                                    <p class="m-b-10"><strong>Order Status : </strong> <span class="float-right"><span class="badge badge-success">Paid</span></span></p>
                                    <p class="m-b-10"><strong>Order No. : </strong> <span class="float-right">000028 </span></p>
                                 </div>
                              </div>
                              <!-- end col -->
                           </div>
                           <!-- end row -->
                           <div class="row mt-3">
                              <div class="col-sm-6">
                                 <h6>Billing Address</h6>
                                 <address>
                                    Stanley Jones<br>
                                    795 Folsom Ave, Suite 600<br>
                                    San Francisco, CA 94107<br>
                                    <abbr title="Phone">P:</abbr> (123) 456-7890
                                 </address>
                              </div>
                              <!-- end col -->
                              <div class="col-sm-6">
                                 <h6>Shipping Address</h6>
                                 <address>
                                    Stanley Jones<br>
                                    795 Folsom Ave, Suite 600<br>
                                    San Francisco, CA 94107<br>
                                    <abbr title="Phone">P:</abbr> (123) 456-7890
                                 </address>
                              </div>
                              <!-- end col -->
                           </div>
                           <!-- end row -->
                           <div class="row">
                              <div class="col-12">
                                 <div class="table-responsive">
                                    <table class="table mt-4 table-centered">
                                       <thead>
                                          <tr>
                                             <th>#</th>
                                             <th>Item</th>
                                             <th style="width: 10%">Hours</th>
                                             <th style="width: 10%">Hours Rate</th>
                                             <th style="width: 10%" class="text-right">Total</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr>
                                             <td>1</td>
                                             <td>
                                                <b>Web Design</b> <br/>
                                                2 Pages static website - my website
                                             </td>
                                             <td>22</td>
                                             <td>$30</td>
                                             <td class="text-right">$660.00</td>
                                          </tr>
                                          <tr>
                                             <td>2</td>
                                             <td>
                                                <b>Software Development</b> <br/>
                                                Invoice editor software - AB'c Software
                                             </td>
                                             <td>112.5</td>
                                             <td>$35</td>
                                             <td class="text-right">$3937.50</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <!-- end table-responsive -->
                              </div>
                              <!-- end col -->
                           </div>
                           <!-- end row -->
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="clearfix pt-5">
                                    <h6 class="text-muted">Notes:</h6>
                                    <small class="text-muted">
                                    All accounts are to be paid within 7 days from receipt of
                                    invoice. To be paid by cheque or credit card or direct payment
                                    online. If account is not paid within 7 days the credits details
                                    supplied as confirmation of work undertaken will be charged the
                                    agreed quoted fee noted above.
                                    </small>
                                 </div>
                              </div>
                              <!-- end col -->
                              <div class="col-sm-6">
                                 <div class="float-right">
                                    <p><b>Sub-total:</b> <span class="float-right">$4597.50</span></p>
                                    <p><b>Discount (10%):</b> <span class="float-right"> &nbsp;&nbsp;&nbsp; $459.75</span></p>
                                    <h3>$4137.75 USD</h3>
                                 </div>
                                 <div class="clearfix"></div>
                              </div>
                              <!-- end col -->
                           </div>
                           <!-- end row -->
                           <div class="mt-4 mb-1">
                              <div class="text-right d-print-none">
                                 <a href="javascript:window.print()" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-printer mr-1"></i> Print</a>
                                 <a href="#" class="btn btn-info waves-effect waves-light"><i class="mdi mdi-send mr-1"></i> Submit</a>
                              </div>
                           </div>
                        </div>
                        <!-- end card-box -->
                     </div>
                     <!-- end col -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div> <!-- container -->
<script>
   function changeLogo() {
       $('#logo').click();
   }
   $('#logo').change(function () {
       var imgPath = $(this)[0].value;
       var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
       if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
           readURLlogo(this);
       else
           alert("Please select logo file (jpg, jpeg, png).")
   });
   function readURLlogo(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.readAsDataURL(input.files[0]);
           reader.onload = function (e) {
               $('#previewlogo').attr('src', e.target.result);
               $('#removelogo').val(0);
           }
       }
   }
   function removeLogo() {
       $('#previewlogo').attr('src', '{{url('images/noname.jpg')}}');
       $('#removelogo').val(1);
   }

   function changeFevicon() {
       $('#fevicon').click();
   }
   $('#fevicon').change(function () {
       var imgPath = $(this)[0].value;
       var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
       if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg")
           readURLfevicon(this);
       else
           alert("Please select fevicon file (jpg, jpeg, png).")
   });
   function readURLfevicon(input) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.readAsDataURL(input.files[0]);
           reader.onload = function (e) {
               $('#previewfevicon').attr('src', e.target.result);
               $('#removefevicon').val(0);
           }
       }
   }
   function removeFevicon() {
       $('#previewfevicon').attr('src', '{{url('images/noname.jpg')}}');
       $('#removefevicon').val(1);
   }

</script>
@endsection
@section('script')
<!-- Tost-->
<script src="{{ URL::asset('assets/libs/jquery-toast/jquery-toast.min.js')}}"></script>
<!-- toastr init js-->
<script src="{{ URL::asset('assets/js/pages/toastr.init.js')}}"></script>
@endsection
