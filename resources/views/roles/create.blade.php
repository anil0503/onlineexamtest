@extends('layouts.master')

@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                                <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">Create New Role</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

   	@if (count($errors) > 0)
       <div class="alert alert-danger">
           <strong>Whoops!</strong> There were some problems with your input.<br><br>
           <ul>
               @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
               @endforeach
           </ul>
       </div>
   @endif
   {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
   <div class="row">
       <div class="col-8">
               <div class="col-xs-12 col-sm-12 col-md-12">
                       <div class="form-group">
                           <strong>Name:</strong>
                           {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                       </div>
                   </div>
                   <div class="col-xs-12 col-sm-12 col-md-12">
                       <div class="form-group">
                           <strong>Display Name:</strong>
                           {!! Form::text('display_name', null, array('placeholder' => 'Display Name','class' => 'form-control')) !!}
                       </div>
                   </div>
                   <div class="col-xs-12 col-sm-12 col-md-12">
                       <div class="form-group">
                           <strong>Description:</strong>
                           {!! Form::textarea('description', null, array('placeholder' => 'Description','class' => 'form-control','style'=>'height:100px')) !!}
                       </div>
                   </div>
       </div>
       <div class="col-4">
               <div class="form-group">
                       <strong>Permission:</strong>
                       <br/>
                       @foreach($permission as $value)
                           <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                           {{ $value->display_name }}</label>
                           <br/>
                       @endforeach
                   </div>
           </div>
   </div>
   <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button type="submit" class="btn btn-primary">Submit</button>
        </div>
	</div>
	{!! Form::close() !!}



                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->




                    </div> <!-- container -->
@endsection

@section('script')

        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection







{{-- @extends('layouts.master')

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">
<br>
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="pull-right">
                                    </div>
                                    <h4 class="page-title"></h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                    </div> <!-- container -->





    @endsection --}}
