@extends('layouts.user')
@section('css')
        <!-- Animation css -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />

        <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<link href="{{ URL::asset('css/verification.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.typekit.net/zyh0use.css">


<script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js')}}"></script>

<script type="text/javascript" src="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>


<script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js')}}"></script>


<!-- MultiStep Form -->
<div class="row">
     <div class="col-md-2">
     </div>
    <div class="col-md-8">

        <div id="msform">
            <!-- progressbar -->
            <ul id="progressbar">

                <li class="active">Business Details</li>
                <li class="active1">Create Board</li>
                <li class="active1">Create Post</li>
                <li class="active4">Send Invites</li>
            </ul>
            <!-- fieldsets -->
        <fieldset>
            <h2 class="fs-title">Send Invites</h2>
                <h3 class="fs-subtitle">Provide valuable, actionable feedback to your users with HTML5 form validation–available in all our supported browsers.

</h3>
 
  <div class="container box">
   @if (count($errors) > 0)
    <div class="alert alert-danger">
     <button type="button" class="close" data-dismiss="alert">×</button>
     <ul>
      @foreach ($errors->all() as $error)
       <li>{{ $error }}</li>
      @endforeach
     </ul>
    </div>
   @endif
   @if ($message = Session::get('success'))
   <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
           <strong>{{ $message }}</strong>
   </div>
   @endif
   <form method="post" action="{{url('sendemail/send')}}">
    {{ csrf_field() }}
 
    <div class="form-group">
     <label>Enter Your Email</label>
     <input type="text" name="email" class="form-control" placeholder="Enter E-Mail For Invite" value="" />
    </div>

    <div class="form-group">
        <a href="{{action('OnboardController@sendinvites')}}" class="action-button">Skip Invitations</a>

     <input type="submit" name="send" class="action-button" value="Send" />
    </div>
   </form>
   
  </div>
 </div>

    </div>
    <div class="col-md-2">
     </div>
</div>



<script type="text/javascript" src="{{ URL::asset('js/verification.js')}}"></script> 
<style>
body{
    background:#f7f7f7;
}
#progressbar li.active1:before, #progressbar li.active:after {
    background: #443c8f;
    color: white;
}
#progressbar li.active2:before, #progressbar li.active:after {
    background: #443c8f;
    color: white;
}
#progressbar li.active3:before, #progressbar li.active:after {
    background: #443c8f;
    color: white;
}
.footer {
    bottom: 0;
    padding: 19px 15px 20px;
    position: fixed;
    right: 0;
    color: #98a6ad;
    left: 0;
    background-color: #eeeff3;
}
</style>
<script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<div class="form-row" style="margin-right:-18px;" id="row'+i+'"><div class="form-group col-11"><input type="email" name="name[]" placeholder="Enter E-Mail For Invite" class="form-control" /></div><div form-group col-md-4><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div>');  
      });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      });  
 });  
 </script>
@endsection