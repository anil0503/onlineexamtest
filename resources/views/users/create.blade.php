@extends('layouts.vertical.master')
@section('content')

<div class="row">
        <div class="col-12">
           <div class="page-title-box">
              <div class="page-title-right">
                    <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>

              </div>
              <h4 class="page-title">Add New User</h4>

           {{-- <div class="pull-right">
        </div> --}}
    </div>
        </div>
     </div>
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="card">
        <div class="card-body">
<form action="{{ route('users.store') }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Password:</strong>
                <input id="password" type="password" class="form-control" name="password" required>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>User Type:</strong>
                <select class="form-control" id="user_type" name="user_type">
                        <option>Select User Type</option>
                        <option value="Customer">Customer</option>
                        <option value="Super Admin">Super Admin</option>
                    </select>
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>
        </div></div>
@endsection
