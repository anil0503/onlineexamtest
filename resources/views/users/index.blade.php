@extends('layouts.vertical.master')
@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                                <div class="col-12">
                                   <div class="page-title-box">
                                      <div class="page-title-right">
                                            <a class="btn btn-success" href="{{ route('users.create') }}"> Create New user</a>

                                      </div>
                                      <h4 class="page-title">User Managment</h4>

                            </div>
                                </div>
                             </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
@if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif

                                        <table id="basic-datatable" class="table dt-responsive nowrap">
                                                <thead><tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>User Type</th>
                                                        <th width="280px">Action</th>
                                                    </tr>  </thead>

                                                    <tbody>
                                                            @foreach ($users as $user)
                                                            <tr>
                                                        <td>{{ $user->id }}</td>
                                                        <td>{{ $user->name }}</td>
                                                        <td>{{ $user->email }}</td>
                                                        <td>{{ $user->user_type }}</td>
                                                        <td>
                                                            <form action="{{ route('users.destroy',$user->id) }}" method="POST">

                                                                {{-- <a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a> --}}

                                                                <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>

                                                                @csrf
                                                                @method('DELETE')

                                                                <button type="submit" class="btn btn-danger">Delete</button>
                                                            </form>
                                                        </td>
                                                    </tr> @endforeach</tbody>




                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->




                    </div> <!-- container -->
@endsection

@section('script')

        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection
