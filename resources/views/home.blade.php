@extends('layouts.vertical.master')

@section('css')
        <!-- Animation css -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />

        <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<p>{{$posts}}</p>
 <!-- Plugins css -->
                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">

                                    </div>

           <h4 class="page-title">Let us know where we can improve.                                        </h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col-md-8">
                                    <div class="card-box1">

                                            <ul class="nav nav-tabs nav-bordered nav-justified">
                                                <li class="nav-item">
                                                    <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link  tab-back active">
                                                        All
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#home-b2" data-toggle="tab" aria-expanded="true" class="nav-link tab-back">
                                                        Planned
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                        In Progress
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                        <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                                Completed
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
<a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
    Expired
</a>
                                                        </li>
                                            </ul>
                                            <br>
                                            <div   class="row tab-items">
                                            <div  style="padding-bottom:10px;"  class="col-xl-3">
                                                    <div class="dropdown bootstrap-select show-tick">
                                                    <select class="selectpicker" multiple="" data-selected-text-format="count" data-style="btn-light" tabindex="-98">
                          @foreach ($boards as $board)
                              {{-- {{ $board->id }} --}}
                               @if($loop->index == 0)
                                <option selected> {{$board->name}}</option>
                               @else
                            <option> {{$board->name}}</option>
                               @endif

                            @endforeach
                            </select>
                                                        </div>

                                            </div>
                                             <div style="padding-bottom:10px;"  class="col-xl-6">
                                                    <div class="input-group clockpicker">
<input style="border:2px red;background-color:#eeeeee" type="text" class="form-control" id="search" placeholder="Search Here">
<div class="input-group-append">
    <span style="border:2px red; background-color:#eeeeee" class="input-group-text"><i class="fe-search"></i></span>
</div>
                                                    </div>
                                            </div>
                                            <div  class="col-xl-3">
                                                    <div class="dropdown bootstrap-select show-tick"><select class="selectpicker" multiple="" data-selected-text-format="count > 3" data-style="btn-light" tabindex="-98">
<option>Mustard</option>
<option>Ketchup</option>
<option>Relish</option>
<option>Onions</option>
                                                        </select>

                                                    </div>
                                            </div>

                                            <div class="col-xl-3">

                                            </div>
                                            </div>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="home-b2">




@foreach($posts as $post)
 <div style="margin-top:-10px; margin-bottom:-10px; border-bottom: 1px solid #dddddd;">
                                                        <div style="padding:0px 0px 10px 20px; " class="row">
    <div class="col-1">
<div class="triangle-up">

    </div>
    <p style=" align:center; padding-left:4px; padding-top:10px;"> 25 </p>
    </div>

    <div class="col-10">

    <div>
<h4><a class="text-body " href="{{ URL::asset('/singleboard')}}">{{$post->title}} </a></h4>
        <div  class="row">
<div style="padding-left:10px;" >
        <div  class="fif fa-hover"><div class="circle fif"> </div>
        <div style="color:#443c8f; padding-left:6px; color:443c8f;" class="fif">{{$post->status}}</div></div>

</div>
&ensp;  &emsp;
<div>
        <div class=" fif fa-hover"><a href="#/comment"><div class="circle fif"> </div> </a>                      </div>
        <div class="fif"> Brand Name </div>
</div>

        </div>

        <p  class="comment more" style="padding-top:5px;">{{$post->body}}</p>


        <div class="fif fa-hover"><a href="#/comment"><i style="color:#20DDC6" ;="" class="fif fa fa-comment"></i> </a>  </div>
        <div class="fif"> 15 Comments </div>
        <div style="padding-left:10px;" class=" fif fa-hover"><a href="#/comment"><i style="color:#443c8f" ;="" class="fif fa fa-adjust"></i> </a>                      </div>
        <div class="fif"> 75% Priyorty </div>


      </div>
    </div>
    <div class="col-1">
    </div>
    </div>
</div>
<br>
@endforeach



                                                   <div class="tab-items row">
                                                          <div class="col-sm-12 col-md-5">
    <div class="dataTables_info" id="row-callback-datatable_info" role="status" aria-live="polite">
        Showing 1 to 3 of 57 Posts
    </div>
</div>
<div class="col-sm-12 col-md-2">
</div>
<div class="col-sm-12 col-md-5">
    <div class="dataTables_paginate paging_simple_numbers" id="row-callback-datatable_paginate">
        <ul class="pagination pagination-rounded"><li class="paginate_button page-item previous disabled" id="row-callback-datatable_previous"><a href="#" aria-controls="row-callback-datatable" data-dt-idx="0" tabindex="0" class="page-link"><i class="mdi mdi-chevron-left"></i></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="row-callback-datatable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="row-callback-datatable" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="row-callback-datatable" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="row-callback-datatable" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="row-callback-datatable" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="row-callback-datatable" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="row-callback-datatable_next"><a href="#" aria-controls="row-callback-datatable" data-dt-idx="7" tabindex="0" class="page-link"><i class="mdi mdi-chevron-right"></i></a></li></ul>
    </div>
</div>
                                                        </div>





                                                </div>
                                                <div class="tab-pane" id="profile-b2">
                                                    <p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                                    <p class="mb-0">Vakal text here dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                                                </div>
                                                <div class="tab-pane" id="messages-b2">
                                                    <p>Vakal text here dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                                                    <p class="mb-0">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                                                </div>
                                            </div>
                                        </div>
                            </div> <!-- end col -->
                            <div  style="margin-left:-50px;" class="col-md-1">
                            </div>
                            <div class="col-md-3">

                                    <div class="resp-container1">
                                            <h4  align="center"  style="padding-top:13px; padding-bottom:13px;" align="center">Create a Request</h4>

                                            <textarea style=" font-size:12px; padding-bottom:20px;" id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="15" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" placeholder="Add Tittle and Short Description"></textarea>
                                            <br><textarea style=" font-size:12px; padding-bottom:80px;" id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10" placeholder="Add Additional Details"></textarea>
                                           <br>
                                        <div align="center">    <button  type="button" class="buttonform" data-toggle="modal" data-target=".bs-example-modal-center">Submit</button>
                                        </div>      <br>
                                          </div>




                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                                <div  class="modal-dialog modal-dialog-centered">
                                    <div  class="modal-content">

                                        <div class="modal-body">

                                                <div class="row">
                                                        <div class="col-12">
<div class="text-center">
    <h2 class="mt-0">
        <i class="mdi mdi-check"></i>

    </h2>
    <h3 class="mt-0">Your vote has been recorded</h3>

    <p class="w-75 mb-2 mx-auto">How importent is this to you?<br>
    This will help us priortise better</p>
    <form action="#" id="carform">

                                                <div class="row">
                                                        <div class="col-2">
                                                        </div>
    <div class="col-8">
<div style="background-color:#eeeeee;" class="row">
        <div class="col-9">
                <h5>Need it ASAP!
                    </h5>
        </div>
    <div class="col-3">
            <h4>
                    <i align-items="right" class="mdi mdi-check1"></i>
                </h4>
    </div>
</div>
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
        </div>
    <div  style="padding-top:5px; " class="col-8">
            <div class="row">
                    <div  class="col-9">
                            <h5 style="font-weight:500;">Importent but not urgent
                                </h5>
                    </div>
                <div class="col-3">
                        <h4>
                                <i align-items="right" class="mdi mdi-check2"></i>
                            </h4>
                </div>
            </div>
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
        </div>
                <div  style="padding-top:5px;padding-bottom:15px; " class="col-8">
                        <div  class="row">
                                <div  class="col-9">
                                        <h5>Good to Have</h5>                                            </h4>
                                </div>
                            <div class="col-3">
                                    <h4>
                                            <i align-items="right" class="mdi mdi-check2"></i>
                                        </h4>
                            </div>
                        </div>
                </div>
                <div class="col-2">
                </div>
<div class="col-12">


      <button  type="button" class="buttonform1" data-toggle="modal" data-target=".bs-example-modal-center">Submit</button>

      </div>
</div>
</div>
                                                        </div>
                                                        <!-- end col -->
                                                    </div> </form>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->


                    </div> <!-- container -->

@endsection

@section('script')

        <!-- Magnific Popup-->
        <script src="{{ URL::asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ URL::asset('/js/tribevote.js')}}"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-latest.js"></script>
        <!-- Tour init js-->
        <script src="{{ URL::asset('assets/js/pages/lightbox.init.js')}}"></script>


@endsection

