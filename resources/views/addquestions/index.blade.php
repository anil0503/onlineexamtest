@extends('layouts.vertical.master')
@section('css')
<!-- third party css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">
   <!-- start page title -->

   @if ($message = Session::get('success'))
   <div class="alert alert-success">
      <p>{{ $message }}</p>
   </div>
   @endif
   <div class="row">
        <div class="row">
                <div class="col-12">
                   <div class="page-title-box">
                      <h4 class="page-title">List of Questions</h4> (mapped to quiz)
                   </div>
                </div>
             </div>
      <div class="col-12">
         <div class="card">
            <div class="card-body">
               <table id="basic-datatable" class="table dt-responsive nowrap">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th width="15%">Question</th>
                        <th>Subject</th>
                        <th>category</th>
                        <th>D'Level</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach ($questions as $question)

                     <tr>
                        <td>{{ $question->id }} </td>
                        <td>{!! $question->question !!}</td>
                        <td>{!! $question->subject !!}</td>
                        <td>{!! $question->category !!}</td>
                        <td>{!! $question->level !!}</td>

                    <td>
                            @if($question->id != $question->question_id)
                            <form action="{{ route('addquestions.destroy',$question->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <select class="form-control">
                                                @foreach($quiz as $quizzes)
                                            {{-- @if($quizzes->id == $keys->exam_id) --}}
                                            @foreach($quiz as $quizzess)
                                            <option value="{{$quizzess->id}}"> {{$quizzess->name}} </option>
                                            @endforeach

                                            {{-- @endif --}}
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    <button type="submit" class="btn btn-danger">Delete To Exam</button>
                                    </div>
                                </form>
                                @endif
                            </td>


                        {{-- <td>
                                @if($question->id == $question->question_id)


                                    @else
                                    <form action="{{ route('addquestions.store') }}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <input class="form-control" id="question_id"  type="hidden" name="question_id" value="{{$question->id}}">
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="form-group">
                                                <select class="form-control" name="exam_id">
                                                    <option> Select Exam </option>
                                                    @foreach($quiz as $quizzes)
                                                    <option value="{{$quizzes->id}}"> {{$quizzes->name}} </option>
                                                    @endforeach
                                                </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                                <button type="submit" class="btn btn-primary">Add to Exam</button>
                                            </div>
                                        </div>
                                    </form>
                                    @endif

                                    </td> --}}
                                </tr>
                                @endforeach

                            </tbody>
               </table>
            </div>
            <!-- end card body-->
         </div>
         <!-- end card -->
      </div>
      <!-- end col-->
      <div class="row">
            <div class="col-12">
               <div class="page-title-box">
                  <h4 class="page-title">List of All Questions</h4>
               </div>
            </div>
         </div>
      <div class="col-12">
            <div class="card">
               <div class="card-body">
                  <table id="basic-datatable" class="table dt-responsive nowrap">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th width="15%">Question</th>
                           <th>Subject</th>
                           <th>category</th>
                           <th>D'Level</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach ($questionsadd as $question)

                        <tr>
                           <td>{{ $question->id }} </td>
                           <td>{!! $question->question !!}</td>
                           <td>{!! $question->subject !!}</td>
                           <td>{!! $question->category !!}</td>
                           <td>{!! $question->level !!}</td>

                       <td>
                               @if($question->id != $question->question_id)
                               <form action="{{ route('addquestions.store') }}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <input class="form-control" id="question_id"  type="hidden" name="question_id" value="{{$question->id}}">
                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                            <div class="form-group">
                                            <select class="form-control" name="exam_id">
                                                <option> Select Exam </option>
                                                @foreach($quiz as $quizzes)
                                                <option value="{{$quizzes->id}}"> {{$quizzes->name}} </option>
                                                @endforeach
                                            </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                            <button type="submit" class="btn btn-primary">Add to Exam</button>
                                        </div>
                                    </div>
                                </form>
                                   @endif
                               </td>



                                   </tr>
                                   @endforeach

                               </tbody>
                  </table>
               </div>
               <!-- end card body-->
            </div>
            <!-- end card -->
         </div>
         <!-- end col-->
   </div>
   <!-- end row-->
</div>
<!-- container -->
@endsection
@section('script')
<!-- third party js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<!-- third party js ends -->
<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>
@endsection
