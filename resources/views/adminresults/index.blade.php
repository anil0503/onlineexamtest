@extends('layouts.vertical.master')
@section('css')
<!-- third party css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <h4 class="page-title">Results Managment</h4>

            <br>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-12">
         <div class="card">
            <div class="card-body">

               <table id="basic-datatable" class="table dt-responsive nowrap">
                  <thead>
                        <tr>
                                <th>No</th>
                                <th>Quiz Name</th>
                                <th>Duration</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Results</th>
                             </tr>
                          </thead>
                          <tbody>
                             @foreach ($quizzes as $quiz)
                             <tr>
                                <td>{{ $quiz->id }}</td>
                                <td>{{ $quiz->name }}</td>
                                <td>{{ $quiz->duration }} Minutes</td>
                                <td>{{ $quiz->start_date }}</td>
                                <td>{{ $quiz->end_date }}</td>
                                <td>
                                                                <a class="btn btn-info" href="{{ route('adminresults.show',$quiz->id) }}">View Results</a>
                                                            </td>

                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
            <!-- end card body-->
         </div>
         <!-- end card -->
      </div>
      <!-- end col-->
   </div>
   <!-- end row-->
</div>
<!-- container -->
@endsection
@section('script')
<!-- third party js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<!-- third party js ends -->
<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>
@endsection
