@extends('layouts.vertical.master')
@section('css')
<!-- third party css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<!-- third party css end -->
@endsection
@section('content')
<!-- Start Content-->
<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="card">
            <div class="card-body">


                     @foreach ($quizzes as $quiz)
                     <div class="col-12">
                            <div class="page-title-box">
                               <h4 class="page-title">Results Explanation of {{ $quiz->name }}</h4>

                               <br>
                            </div>
                         </div>

                                    @foreach($results as $question)
                                    <div class="row">
                                       <div class="col-12">
                                          <div class="card">
                                             <div class="card-body">
                                                @if($question->category == "MCQ")
                                                <div >
                                                   <table class="table table-bordered">
                                                      <tbody>
                                                         <tr class="text-info">
                                                         </tr>
                                                         <tr>
                                                            <td colspan="5">
                                                               <strong> Directions</strong>  {!! $question->context !!}
                                                               <strong>Question: </strong>{!! $question->question !!}
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td colspan="5">
                                                               <div class="mt-3">
                                                                  <div class="custom-control custom-radio">
                                                                     <input type="radio" id=" {!! $question->id !!}1" name="ans_1" class="custom-control-input" value="1">
                                                                     <label class="custom-control-label" for=" {!! $question->id !!}1">{!! $question->option_1 !!}</label>
                                                                  </div>
                                                                  <div class="custom-control custom-radio">
                                                                     <input type="radio" id=" {!! $question->id !!}2" name="ans_1" class="custom-control-input" value="2">
                                                                     <label class="custom-control-label" for=" {!! $question->id !!}2">{!! $question->option_2 !!}</label>
                                                                  </div>
                                                                  <div class="custom-control custom-radio">
                                                                     <input type="radio" id=" {!! $question->id !!}3" name="ans_1" class="custom-control-input" value="3">
                                                                     <label class="custom-control-label" for=" {!! $question->id !!}3">{!! $question->option_3 !!}</label>
                                                                  </div>
                                                                  <div class="custom-control custom-radio">
                                                                     <input type="radio" id=" {!! $question->id !!}4" name="ans_1" class="custom-control-input" value="4">
                                                                     <label class="custom-control-label" for=" {!! $question->id !!}4">{!! $question->option_4 !!}</label>
                                                                  </div>
                                                                  <div class="custom-control custom-radio">
                                                                     <input type="radio" id=" {!! $question->id !!}5" name="ans_1" class="custom-control-input" value="5">
                                                                     <label class="custom-control-label" for=" {!! $question->id !!}5">{!! $question->option_5 !!}</label>
                                                                  </div>
                                                                  <div class="custom-control custom-radio">
                                                                     <input type="radio" id=" {!! $question->id !!}6" name="ans_1" class="custom-control-input" value="6">
                                                                     <label class="custom-control-label" for=" {!! $question->id !!}6">{!! $question->option_6 !!}</label>
                                                                  </div>
                                                               </div>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td><strong>Submited Answer</strong> {{$question->answers }}</td>
                                                            <td><strong>Marks :</strong> {!! $question->marks !!}</td>
                                                            <td><strong>Negative Marks :</strong> {!! $question->negative !!}</td>
                                                            <td><strong>Subject :</strong> {!! $question->subject !!}</td>
                                                            <td><strong>Dificulty Level :</strong> {!! $question->level !!}</td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                                @endif
                                                @if($question->category == "blanks")
                                                <div >
                                                   <table class="table table-bordered">
                                                      <tbody>
                                                         <tr class="text-info">
                                                         </tr>
                                                         <tr>
                                                            <td colspan="5">
                                                               <strong> Directions</strong>  {!! $question->context !!}
                                                               <strong>Question: </strong>{!! $question->question !!}
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td><strong>Submited Answer</strong> {{$question->answers }}</td>
                                                            <td><strong>Marks :</strong> {!! $question->marks !!}</td>
                                                            <td><strong>Negative Marks :</strong> {!! $question->negative !!}</td>
                                                            <td><strong>Subject :</strong> {!! $question->subject !!}</td>
                                                            <td><strong>Dificulty Level :</strong> {!! $question->level !!}</td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                                @endif
                                                @if($question->category == "voice")
                                                <div >
                                                   <table class="table table-bordered">
                                                      <tbody>
                                                         <tr class="text-info">
                                                         </tr>
                                                         <tr>
                                                            <td colspan="5">
                                                               <strong> Directions</strong>  {!! $question->context !!}
                                                               <strong>Question: </strong>{!! $question->question !!}
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td><strong>Submited Answer</strong> {{$question->answers }}</td>
                                                            <td><strong>Marks :</strong> {!! $question->marks !!}</td>
                                                            <td><strong>Negative Marks :</strong> {!! $question->negative !!}</td>
                                                            <td><strong>Subject :</strong> {!! $question->subject !!}</td>
                                                            <td><strong>Dificulty Level :</strong> {!! $question->level !!}</td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                                @endif
                                                </p>
                                             </div>
                                             <!-- end card body-->
                                          </div>
                                          <!-- end card -->
                                       </div>
                                       <!-- end col-->
                                    </div>
                                    @endforeach

                     @endforeach
                  </tbody>
               </table>
            </div>
            <!-- end card body-->
         </div>
         <!-- end card -->
      </div>
      <!-- end col-->
   </div>
   <!-- end row-->
</div>
<!-- container -->
@endsection
@section('script')
<!-- third party js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
<script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
<!-- third party js ends -->
<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>
@endsection
