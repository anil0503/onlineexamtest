        <!-- Vendor js -->

  <!-- Vendor js -->
  <script src="{{ URL::asset('assets/js/vendor.min.js')}}"></script>
  <script src="{{ URL::asset('vertical/assets/libs/jquery-nice-select/jquery.nice-select.min.js')}}"></script>
  <script src="{{ URL::asset('vertical/assets/libs/switchery/switchery.min.js')}}"></script>
  <script src="{{ URL::asset('vertical/assets/libs/select2/select2.min.js')}}"></script>
  <script src="{{ URL::asset('vertical/assets/libs/bootstrap-select/bootstrap-select.min.js')}}"></script>
  <script src="{{ URL::asset('vertical/assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>
  <script src="{{ URL::asset('vertical/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

  <script src="{{ URL::asset('assets/libs/moment/moment.min.js')}}"></script>
  <script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script>
  {{-- <script src="{{ URL::asset('assets/libs/clockpicker/clockpicker.min.js')}}"></script> --}}
  <script src="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
  {{-- <script src="{{ URL::asset('assets/libs/daterangepicker/daterangepicker.min.js')}}"></script> --}}
  {{-- <script src="{{ URL::asset('assets/js/pages/form-pickers.init.js')}}"></script> --}}




<!-- Init js-->
<script src="{{ URL::asset('horizontal/assets/js/pages/form-advanced.init.js')}}"></script>


        <!-- Magnific Popup-->
        <script src="{{ URL::asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

        <!-- Tour init js-->
        <script src="{{ URL::asset('assets/js/pages/lightbox.init.js')}}"></script>




        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('vertical/assets/js/app.min.js')}}"></script>

        @yield('script-bottom')
