
        @yield('css')
        <link rel="stylesheet" href="https://use.typekit.net/zyh0use.css">
        <link rel="stylesheet"  href="{{ URL::asset('css/tribevote.css')}}">
        <meta name="google-site-verification" content="glE3MGQJAxKs7o-ovcR6aeMxEdj2DGkAywqTH4-ROpI" />
        <!-- App css -->
        <link href="{{ URL::asset('vertical/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('vertical/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('vertical/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/clockpicker/clockpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/libs/daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
