<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
                <div class="text-center w-75 m-auto">
                        <img src="{{ URL::asset('assets/images/users/user-1.jpg')}}" width="88" alt="user-image" class="rounded-circle img-thumbnail">

                    </div>
            @if(Auth::user()->user_type=="Super Admin")
            <ul class="metismenu" id="side-menu">

                <li>
                    <a href="{{ URL::asset('dashboard')}}">
                        <i class="la la-dashboard"></i>
                        <span> DashBoard </span>
                    </a>
                </li>

                <li>
                        <a href="{{ URL::asset('quizzes')}}">
                            <i class="la la-home"></i>
                            <span> Quizzes </span>
                        </a>
                    </li>

                    <li>
                            <a href="{{ URL::asset('questions')}}">
                                <i class="la la-road"></i>
                                <span> Questions </span>
                            </a>
                        </li>
                        <li>
                                <a href="{{ URL::asset('addquestions')}}">
                                    <i class="la la-road"></i>
                                    <span> Add Questions to Exam</span>
                                </a>
                            </li>

                        <li>
                                <a href="{{ URL::asset('admresults')}}">
                                        <i class="la la-pie-chart"></i>
                                    <span> Results </span>
                                </a>
                            </li>
                            <li>
                                    <a href="{{ URL::asset('users')}}">
                                            <i class="la la-users"></i>
                                    <span> User Manegment </span>
                                </a>
                            </li>
            </ul>
            @else
            <ul class="metismenu" id="side-menu">

                <li>
                    <a href="{{ URL::asset('user/dashboard')}}">
                        <i class="la la-dashboard"></i>
                        <span> DashBoard </span>
                    </a>
                </li>

                <li>
                        <a href="{{ URL::asset('exams')}}">
                            <i class="la la-home"></i>
                            <span> Quizzes </span>
                        </a>
                    </li>


                        <li>
                                <a href="{{ URL::asset('results')}}">
                                        <i class="la la-pie-chart"></i>
                                    <span> Results </span>
                                </a>
                            </li>
            </ul>
            @endif
        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
@if(Auth::user()->user_type=="Super Admin")
<style>
    .left-side-menu {
    width: 240px;
    background: #414d5f;
    bottom: 0;
    padding: 20px 0;
    position: fixed;
    padding-top: 70px;
    box-shadow: 0 0 35px 0 rgba(154, 161, 171, 0.3);
}
</style>
@else
<style>
        .left-side-menu {
        width: 240px;
        background: #08c;
        bottom: 0;
        padding: 20px 0;
        position: fixed;
        padding-top: 70px;
        box-shadow: 0 0 35px 0 rgba(154, 161, 171, 0.3);
    }
    </style>
    @endif
