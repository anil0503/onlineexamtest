<!DOCTYPE html>
<html lang="en">
    <head>
         <!-- //Font Awesome Icons -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/js/all.js"></script>
 <!-- //Font Materials Icons -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css" rel="stylesheet" type="text/css" />
         <!-- //Font Simple Icons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

			<link rel="stylesheet" href="{{ URL::asset('fonts/line-awesome/css/line-awesome.css')}}">
		<link rel="stylesheet" href="{{ URL::asset('fonts/feather-font-master/src/css/iconfont.css')}}">

        <link rel="stylesheet" href="{{ URL::asset('vertical/assets/libs/jquery-toast/jquery-toast.min.css')}}">
        <meta charset="utf-8" />
        <title>OnlineExam</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
 <!-- App favicon -->
 <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico')}}">

 <!-- Plugins css -->
 <link href="{{ URL::asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css')}}" rel="stylesheet" type="text/css" />

 <!-- Animation css -->
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />

 <link href="{{ URL::asset('assets/libs/magnific-popup/magnific-popup.min.css')}}" rel="stylesheet" type="text/css" />



        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('vertical/assets/images/favicon.ico')}}">
        @include('layouts.vertical.head')

    </head>


    <body>

          <!-- Begin page -->
          <div id="wrapper">
      @include('layouts.vertical.topbar')
      @include('layouts.vertical.sidebar')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
      @yield('content')
      {{-- @include('layouts.vertical.right-sidebar') --}}
                </div> <!-- content -->
    @include('layouts.vertical.footer')
            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
    </div>
    <!-- END wrapper -->
    @include('layouts.vertical.footer-script')
    <style>
body {
    font-family: 'Proxima-Nova';
}

            #sidebar-menu > ul > li > a {
    color: #dde5ec;
    display: block;
    font-family: 'Proxima-Nova';
            }
            .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
    margin: 10px 0;
    margin-top: 10px;
    margin-right: 0px;
    margin-bottom: 10px;
    margin-left: 0px;
    font-weight: 500;
    font-family: 'Proxima-Nova';
    color: #414d5f;
}
.navbar-custom {
    background-color: #f7f7f7;
    z-index: 100;
    }


            </style>


    </body>
</html>
