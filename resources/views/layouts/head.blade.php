


         <!-- //Font Awesome Icons -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/js/all.js"></script>
 <!-- //Font Materials Icons -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css" rel="stylesheet" type="text/css" />
         <!-- //Font Simple Icons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">

                <link rel="stylesheet"  href="{{ URL::asset('css/tribevote.css')}}">

		<link rel="stylesheet" href="{{ URL::asset('fonts/line-awesome/css/line-awesome.css')}}">
		<link rel="stylesheet" href="{{ URL::asset('fonts/feather-font-master/src/css/iconfont.css')}}">
	<meta name="google-site-verification" content="glE3MGQJAxKs7o-ovcR6aeMxEdj2DGkAywqTH4-ROpI" />
 <!-- Plugins css -->
 <link href="{{ URL::asset('assets/libs/jquery-nice-select/jquery-nice-select.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/switchery/switchery.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
 <link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css')}}" rel="stylesheet" type="text/css" />


	@yield('css')

        <!-- App css -->
        <link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />
