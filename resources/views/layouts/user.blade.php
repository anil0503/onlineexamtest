<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Online Exam</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="stylesheet" href="https://use.typekit.net/zyh0use.css">

         <!-- //Font Awesome Icons -->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/js/all.js"></script>
         <!-- //Font Materials Icons -->
                <link href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.6.95/css/materialdesignicons.css" rel="stylesheet" type="text/css" />
                 <!-- //Font Simple Icons -->
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">


                <link rel="stylesheet" href="{{ URL::asset('fonts/line-awesome/css/line-awesome.css')}}">
                <link rel="stylesheet" href="{{ URL::asset('fonts/feather-font-master/src/css/iconfont.css')}}">




        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico')}}">
        @include('layouts.head')
        <style>


              </style>
    </head>

    <body>
        <!-- Navigation Bar-->
        <header id="topnav">

                <!-- Topbar Start -->
                <div class="navbar-custom">
                    <div class="container-fluid">
                        <ul class="list-unstyled topnav-menu float-right mb-0">
                                <ul class="navbar-nav ml-auto">
                                        <!-- Authentication Links -->
                                        @guest
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                            </li>
                                            @if (Route::has('register'))
                                                <li class="nav-item">
                                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                                </li>
                                            @endif
                                        @else
                                            <li class="nav-item dropdown">
                                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                    {{ Auth::user()->name }} <span class="caret"></span>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </li>
                                        @endguest
                                    </ul>

                        </ul>

                        <!-- LOGO -->
                        <div  class="logo-box">
                            <a href="{{ URL::asset('/')}}" class="logo text-center">

                                <span class="logo-lg ">
                                    <img src="{{ URL::asset('/images/logo-sm.png')}}" alt="" height="65">
                                    <!-- <span class="logo-lg-text-dark">Xeria</span> -->
                                </span>
                                <span class="logo-sm">
                                    <!-- <span class="logo-sm-text-dark">X</span> -->
                                    <img src="{{ URL::asset('images/logo.png')}}" alt="" height="18">
                                </span>
                            </a>
                        </div>


                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- end Topbar -->

                <div class="topbar-menu">

                    <!-- end container -->
                </div>
                <!-- end navbar-custom -->

            </header>
        <!-- Begin page -->
        <div class="wrapper">
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            @yield('content')
            @include('layouts.right-sidebar')
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        @include('layouts.footer')
        <!-- END wrapper -->
        @include('layouts.footer-script')
    </body>


</html>
