        <!-- Navigation Bar-->
        <header id="topnav">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                        <li class="d-none d-sm-block">
                            <form class="app-search">
                                <div class="app-search-box">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <div class="input-group-append">
                                            <button class="btn" type="submit">
                                                <i class="fe-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </li>

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle  waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i class="fe-bell noti-icon"></i>
                                <span class="badge badge-danger rounded-circle noti-icon-badge">5</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="m-0">
                                        <span class="float-right">
                                            <a href="" class="text-muted">
                                                <small>Clear All</small>
                                            </a>
                                        </span>Notification
                                    </h5>
                                </div>

                                <div class="slimscroll noti-scroll">

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item active">
                                        <div class="notify-icon">
                                            <img src="{{ URL::asset('horizontal/assets/images/users/user-1.jpg')}}" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Cristina Pride</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Hi, How are you? What about our next meeting</small>
                                        </p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-primary">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">1 min ago</small>
                                        </p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon">
                                            <img src="{{ URL::asset('horizontal/assets/images/users/user-4.jpg')}}" class="img-fluid rounded-circle" alt="" /> </div>
                                        <p class="notify-details">Karen Robinson</p>
                                        <p class="text-muted mb-0 user-msg">
                                            <small>Wow ! this admin looks good and awesome design</small>
                                        </p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning">
                                            <i class="mdi mdi-account-plus"></i>
                                        </div>
                                        <p class="notify-details">New user registered.
                                            <small class="text-muted">5 hours ago</small>
                                        </p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info">
                                            <i class="mdi mdi-comment-account-outline"></i>
                                        </div>
                                        <p class="notify-details">Caleb Flakelar commented on Admin
                                            <small class="text-muted">4 days ago</small>
                                        </p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-secondary">
                                            <i class="mdi mdi-heart"></i>
                                        </div>
                                        <p class="notify-details">Carlos Crouch liked
                                            <b>Admin</b>
                                            <small class="text-muted">13 days ago</small>
                                        </p>
                                    </a>
                                </div>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item text-center text-primary notify-item notify-all">
                                    View all
                                    <i class="fi-arrow-right"></i>
                                </a>

                            </div>
                        </li>

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="{{ URL::asset('horizontal/assets/images/users/user-1.jpg')}}" alt="user-image" class="rounded-circle">
                                <span class="pro-user-name ml-1">
                                    Marcia J. <i class="mdi mdi-chevron-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="m-0">
                                        Welcome !
                                    </h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-user"></i>
                                    <span>My Account</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-settings"></i>
                                    <span>Settings</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-lock"></i>
                                    <span>Lock Screen</span>
                                </a>

                                <div class="dropdown-divider"></div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fe-log-out"></i>
                                    <span>Logout</span>
                                </a>

                            </div>
                        </li>

                        <li class="dropdown notification-list">
                            <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect">
                                <i class="fe-settings noti-icon"></i>
                            </a>
                        </li>

                    </ul>

                    <!-- LOGO -->
                    <div class="logo-box">
                        <a href="/" class="logo text-center">
                            <span class="logo-lg">
                                <img src="{{ URL::asset('{{ URL::asset('images/AEC-Logo.png')}}" a')}}lt="" height="60">
                                <!-- <span class="logo-lg-text-dark">Xeria</span> -->
                            </span>
                            <span class="logo-sm">
                                <!-- <span class="logo-sm-text-dark">X</span> -->
                                <img src="{{ URL::asset('{{ URL::asset('images/AEC-Logo.png')}}" a')}}lt="" height="60">
                            </span>
                        </a>
                    </div>

                    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">

                        <li class="dropdown d-none d-lg-block">
                            <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                Reports
                                <i class="mdi mdi-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu">
                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">
                                    Finance Report
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">
                                    Monthly Report
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">
                                    Revenue Report
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">
                                    Settings
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item">
                                    Help & Support
                                </a>

                            </div>
                        </li>

                        <li class="dropdown dropdown-mega d-none d-lg-block">
                            <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                Mega Menu
                                <i class="mdi mdi-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-megamenu">
                                <div class="row">
                                    <div class="col-sm-8">

                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5 class="text-dark mt-0">UI Components</h5>
                                                <ul class="list-unstyled megamenu-list mt-2">
                                                    <li>
                                                        <a href="javascript:void(0);">Widgets</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Nestable List</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Range Sliders</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Masonry Items</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Sweet Alerts</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Treeview Page</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Tour Page</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-4">
                                                <h5 class="text-dark mt-0">Applications</h5>
                                                <ul class="list-unstyled megamenu-list mt-2">
                                                    <li>
                                                        <a href="javascript:void(0);">Email Pages</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Profile</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Calendar</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Team Contacts</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Maintenance</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Coming Soon Page</a>
                                                    </li>
                                                </ul>
                                            </div>

                                            <div class="col-md-4">
                                                <h5 class="text-dark mt-0">Layouts</h5>
                                                <ul class="list-unstyled megamenu-list mt-2">
                                                    <li>
                                                        <a href="javascript:void(0);">Small Sidebar</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Light Sidebar</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Dark Topbar</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Preloader</a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:void(0);">Sidebar Collapsed</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="text-center mt-3">
                                            <h3 class="text-dark">Launching Discount Sale!</h3>
                                            <p class="font-16">Save up to 55% off.</p>
                                            <button class="btn btn-primary mt-1">Download Now <i class="mdi mdi-arrow-right-bold-outline ml-1"></i></button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- end Topbar -->

            <div class="topbar-menu">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu">

                            <li class="has-submenu">
                                <a href="#">
                                    <i class="la la-dashboard"></i>Dashboards <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="index">Dashboard 1</a>
                                    </li>
                                    <li>
                                        <a href="dashboard-2">Dashboard 2</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#">
                                    <i class="la la-cube"></i>Apps <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="apps-calendar">Calendar</a>
                                    </li>
                                    <li>
                                        <a href="apps-contacts">Contacts</a>
                                    </li>
                                    <li>
                                        <a href="apps-tickets">Tickets</a>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#">Email <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="email-inbox">Inbox</a>
                                            </li>
                                            <li>
                                                <a href="email-read">Read Email</a>
                                            </li>
                                            <li>
                                                <a href="email-compose">Compose Email</a>
                                            </li>
                                            <li>
                                                <a href="email-templates">Email Templates</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"> <i class="la la-clone"></i>Layouts <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="layouts-topbar-dark">Topbar Dark</a>
                                    </li>
                                    <li>
                                        <a href="layouts-menubar-light">Menubar Light</a>
                                    </li>
                                    <li>
                                        <a href="layouts-center-menu">Center Menu</a>
                                    </li>
                                    <li>
                                        <a href="layouts-preloader">Preloader</a>
                                    </li>
                                    <li>
                                        <a href="layouts-normal-header">Unsticky Header</a>
                                    </li>
                                    <li>
                                        <a href="layouts-boxed">Boxed</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"> <i class="la la-briefcase"></i>Elements <div class="arrow-down"></div></a>
                                <ul class="submenu megamenu">
                                    <li>
                                        <ul>
                                            <li>
                                                <a href="ui-buttons">Buttons</a>
                                            </li>
                                            <li>
                                                <a href="ui-cards">Cards</a>
                                            </li>
                                            <li>
                                                <a href="ui-tabs-accordions">Tabs & Accordions</a>
                                            </li>
                                            <li>
                                                <a href="ui-modals">Modals</a>
                                            </li>
                                            <li>
                                                <a href="ui-progress">Progress</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li>
                                                <a href="ui-notifications">Notifications</a>
                                            </li>
                                            <li>
                                                <a href="ui-general">General UI</a>
                                            </li>
                                            <li>
                                                <a href="ui-typography">Typography</a>
                                            </li>
                                            <li>
                                                <a href="ui-grid">Grid</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#">
                                    <i class="la la-diamond"></i>Components <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li class="has-submenu">
                                        <a href="#"><i class="fe-bookmark mr-1"></i> Forms <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="forms-elements">General Elements</a>
                                            </li>
                                            <li>
                                                <a href="forms-advanced">Advanced</a>
                                            </li>
                                            <li>
                                                <a href="forms-validation">Validation</a>
                                            </li>
                                            <li>
                                                <a href="forms-pickers">Pickers</a>
                                            </li>
                                            <li>
                                                <a href="forms-wizard">Wizard</a>
                                            </li>
                                            <li>
                                                <a href="forms-masks">Masks</a>
                                            </li>
                                            <li>
                                                <a href="forms-summernote">Summernote</a>
                                            </li>
                                            <li>
                                                <a href="forms-quilljs">Quilljs Editor</a>
                                            </li>
                                            <li>
                                                <a href="forms-file-uploads">File Uploads</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#"><i class="fe-grid mr-1"></i> Tables <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="tables-basic">Basic Tables</a>
                                            </li>
                                            <li>
                                                <a href="tables-datatables">Data Tables</a>
                                            </li>
                                            <li>
                                                <a href="tables-editable">Editable Tables</a>
                                            </li>
                                            <li>
                                                <a href="tables-responsive">Responsive Tables</a>
                                            </li>
                                            <li>
                                                <a href="tables-tablesaw">Tablesaw Tables</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#"><i class="fe-bar-chart-2 mr-1"></i> Charts <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="charts-apex">Apex Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-flot">Flot Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-morris">Morris Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-chartjs">Chartjs Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-brite">Brite Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-chartist">Chartist Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-peity">Peity Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-sparklines">Sparklines Charts</a>
                                            </li>
                                            <li>
                                                <a href="charts-knob">Jquery Knob Charts</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#"><i class="fe-cpu mr-1"></i> Icons <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="icons-feather">Feather Icons</a>
                                            </li>
                                            <li>
                                                <a href="icons-lineawesome">Line Awesome</a>
                                            </li>
                                            <li>
                                                <a href="icons-mdi">Material Design Icons</a>
                                            </li>
                                            <li>
                                                <a href="icons-font-awesome">Font Awesome</a>
                                            </li>
                                            <li>
                                                <a href="icons-simple-line">Simple Line</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="has-submenu">
                                        <a href="#"><i class="fe-map mr-1"></i> Maps <div class="arrow-down"></div></a>
                                        <ul class="submenu">
                                            <li>
                                                <a href="maps-google">Google Maps</a>
                                            </li>
                                            <li>
                                                <a href="maps-vector">Vector Maps</a>
                                            </li>
                                            <li>
                                                <a href="maps-mapael">Mapael Maps</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"> <i class="la la-flask"></i>Admin UI <div class="arrow-down"></div></a>
                                <ul class="submenu">
                                    <li>
                                        <a href="widgets">Widgets</a>
                                    </li>
                                    <li>
                                        <a href="admin-sweet-alert">Sweet Alert</a>
                                    </li>
                                    <li>
                                        <a href="admin-nestable">Nestable List</a>
                                    </li>
                                    <li>
                                        <a href="admin-range-slider">Range Slider</a>
                                    </li>
                                    <li>
                                        <a href="admin-tour">Tour Page</a>
                                    </li>
                                    <li>
                                        <a href="admin-lightbox">Lightbox</a>
                                    </li>
                                    <li>
                                        <a href="admin-treeview">Treeview</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="has-submenu">
                                <a href="#"> <i class="la la-file-text-o"></i>Pages <div class="arrow-down"></div></a>
                                <ul class="submenu megamenu">
                                    <li>
                                        <ul>
                                            <li>
                                                <a href="pages-starter">Starter</a>
                                            </li>
                                            <li>
                                                <a href="pages-login">Log In</a>
                                            </li>
                                            <li>
                                                <a href="pages-register">Register</a>
                                            </li>
                                            <li>
                                                <a href="pages-recoverpw">Recover Password</a>
                                            </li>
                                            <li>
                                                <a href="pages-lock-screen">Lock Screen</a>
                                            </li>
                                            <li>
                                                <a href="pages-logout">Logout</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li>
                                                <a href="pages-confirm-mail">Confirm Mail</a>
                                            </li>
                                            <li>
                                                <a href="pages-404">Error 404</a>
                                            </li>
                                            <li>
                                                <a href="pages-404-alt">Error 404-alt</a>
                                            </li>
                                            <li>
                                                <a href="pages-500">Error 500</a>
                                            </li>
                                            <li>
                                                <a href="extras-profile">Profile</a>
                                            </li>
                                            <li>
                                                <a href="extras-timeline">Timeline</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <ul>
                                            <li>
                                                <a href="extras-invoice">Invoice</a>
                                            </li>
                                            <li>
                                                <a href="extras-faqs">FAQs</a>
                                            </li>
                                            <li>
                                                <a href="extras-pricing">Pricing</a>
                                            </li>
                                            <li>
                                                <a href="extras-maintenance">Maintenance</a>
                                            </li>
                                            <li>
                                                <a href="extras-coming-soon">Coming Soon</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                        <!-- End navigation menu -->

                        <div class="clearfix"></div>
                    </div>
                    <!-- end #navigation -->
                </div>
                <!-- end container -->
            </div>
            <!-- end navbar-custom -->

        </header>
        <!-- End Navigation Bar-->
