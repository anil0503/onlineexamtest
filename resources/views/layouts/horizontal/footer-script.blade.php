        <!-- Vendor js -->
        <script src="{{ URL::asset('horizontal/assets/js/vendor.min.js')}}"></script>

        @yield('script')

        <!-- App js -->
        <script src="{{ URL::asset('horizontal/assets/js/app.min.js')}}"></script>
        
        @yield('script-bottom')