@extends('layouts.vertical.master')

@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Exam</a></li>
                                            <li class="breadcrumb-item"><a href="javascript: void(0);">Users</a></li>
                                            <li class="breadcrumb-item active">User Manegment</li>
                                        </ol>
                                    </div>
                                    <h4 class="page-title">User Manegment</h4>
                                </div>
                            </div>
                        </div>
                        <!-- end page title -->

                        <div class="row">
                            <div class="col-12">
                                    <a href="#" data-target="#useradd" data-toggle="modal">
                                        <button type="button" class="btn btn-primary">Add New Student/Admin</button></a>
                                    <div id="useradd" class="modal fade show" tabindex="-1" role="dialog" aria-labelledby="full-width-modalLabel" style="padding-right: 17px;" aria-hidden="true">
                                                      <div class="modal-dialog modal-full">
                                                          <div class="modal-content">
                                                              <div class="modal-header tect-center">
                                                                  <h4 class="modal-title " id="full-width-modalLabel">New Location</b></h4>
                                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                              </div>

                                                              <div class="modal-body">
                                                                    <form method="POST" action="{{ route('usercreate') }}">
                                                                            @csrf
                                                                            <div class="form-group row">
                                                                               <label for="name">Full Name</label>
                                                                               <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                                                               @error('name')
                                                                               <span class="invalid-feedback" role="alert">
                                                                               <strong>{{ $message }}</strong>
                                                                               </span>
                                                                               @enderror
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                    <label for="user_type">User Type</label>
                                                                                    <select class="form-control" id="example-select">
                                                                                            <option>Select User Type</option>
                                                                                        <option value="Super Admin">Super Admin</option>
                                                                                            <option value="Customer">Student</option>

                                                                                        </select>
                                                                                    @error('user_type')
                                                                                    <span class="invalid-feedback" role="alert">
                                                                                    <strong>{{ $message }}</strong>
                                                                                    </span>
                                                                                    @enderror
                                                                                 </div>
                                                                            <div class="form-group row">
                                                                               <label for="email">E-Mail Address</label>
                                                                               <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                                                               @error('email')
                                                                               <span class="invalid-feedback" role="alert">
                                                                               <strong>{{ $message }}</strong>
                                                                               </span>
                                                                               @enderror
                                                                            </div>
                                                                            <div class="form-group row">
                                                                               <label for="password">Password</label>
                                                                               <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password">
                                                                               {{-- {!! Form::password('password', ['class' => 'form-control','placeholder' => '*Password']) !!} --}}
                                                                               @error('password')
                                                                               <span class="invalid-feedback" role="alert">
                                                                               <strong>{{ $message }}</strong>
                                                                               </span>
                                                                               @enderror
                                                                            </div>

                                                                            <input id="source" type="hidden" name="source" value="website" required autofocus>

                                                                            <input type="hidden" id="user_type" name="user_type" value="Customer">
                                                                            <div>

                                                                            </div>

                                                              </div>

                                                              <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-danger btn-block">
                                                                            {{ __('Register') }}
                                                                            </button>
                                                            </div>
                                                        </form>

                                                          </div><!-- /.modal-content -->
                                                      </div><!-- /.modal-dialog -->
                                </div>
                                <br><br>

                                <div class="card">
                                    <div class="card-body">
                                        <table id="basic-datatable" class="table dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>email</th>
                                                    <th>User Type</th>
                                                    <th>Created At</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>


                                            @if(count($users)>0)

                                            <tbody>
                                                @foreach($users as $user)
                                                <tr>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->user_type}}</td>
                                                <td>{{$user->created_at}}</td>
                                                <th>Actions</th>
                                                </tr>
                                                @endforeach
                                           </tbody>
                                           @endif

                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->


                    </div> <!-- container -->
@endsection

@section('script')

        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection
