@extends('layouts.vertical.master')
@section('css')
<style>
   a, a:hover {
   color: white;
   }
   .btn-group > .btn, .btn-group-vertical > .btn {
   position: relative;
   flex: 1 1 auto;
   color: #6c757d;
   }
   .note-editor.note-frame .note-editing-area .note-editable {
   height: 200px;
   background-color: #f672a7;
   }
   /* .note-editor {
   height: 350px;
   position: relative;
   } */
</style>
@endsection
@section('content')
<link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
{{--
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
--}}
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<!-- Start Content-->
<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <div class="page-title-right">
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                  <li class="breadcrumb-item active">Create A Question</li>
               </ol>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
               <strong>Whoops!</strong> There were some problems with your input.<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
            @endif
            <h4 class="page-title">Edit Quiz</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="card-box">


                            <div class="row">
                               <div class="col-9">
                                  <div class="form-row">
                                        <div class="form-group col-md-12 required">
                                                {!! Form::label("title","Quiz Title ",["class"=>"col-form-label"]) !!}
                                                <input class="form-control" id="name"  type="text" name="name" value="{{$quiz->name}}" readonly>
                                            </div>
                                     <div>
                                        {!!$quiz->instructions!!}
                                     </div>

                                  </div>
                               </div>
                               <div class="col-3">

                                  <div class="form-group required">
                                     {!! Form::label("startdate","Start Date",["class"=>"col-form-label"]) !!}
                                     <input class="form-control" id="start_date" type="date" name="start_date" value="{{$quiz->start_date}}" readonly>
                                  </div>
                                  <div class="form-group required">
                                     {!! Form::label("enddate","End Date",["class"=>"col-form-label"]) !!}
                                     <input class="form-control" id="end_date" type="date" name="end_date" value="{{$quiz->end_date}}" readonly>

                                  </div>
                                  <div class="form-group required">
                                     {!! Form::label("pass","Pass Percentage",["class"=>"col-form-label"]) !!}
                                     <input class="form-control" id="pass" type="number" name="pass" value="{{$quiz->pass}}" readonly>
                                  </div>
                                  <div class="form-group required">
                                        {!! Form::label("duration","Duration",["class"=>"col-form-label"]) !!}
                                        <input class="form-control" id="duration" type="number" name="duration" value="{{$quiz->duration}}" readonly>
                                     </div>
                                  <div class="form-group required">
                                     {!! Form::label("status","Quiz Status",["class"=>"col-form-label"]) !!}
                                     <select class="form-control" name="status">
                                            <option> {{$quiz->status}} </option>
                                            <option value="Published"> Published </option>
                                            <option value="Unpublished"> Unpublished </option>
                                         </select>                                  </div>
                               </div>
                            </div>

            </div>
         </div>
         <!-- end card-box-->
      </div>
   </div>
   <!-- end page title -->

</div>
<!-- end row -->

<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
            @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <a class="btn btn-success" href="{{ asset('addquestions') }}"> Add/Remove Questions</a>
<br><br>
                <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead><tr>
                                    <th>No</th>
                                    <th width="15%">Question</th>
                                    <th>+ve Marks</th>
                                    <th>-ve Marks</th>
                                    <th>Subject</th>
                                    <th>category</th>
                                    <th>D'Level</th>

                                </tr>  </thead>

                                <tbody>
                                        @foreach ($questions as $question)
                                        <tr>
                                    <td>{{ $question->id }}</td>
                                    <td>{!! $question->question !!}</td>
                                    <td>{!! $question->marks !!}</td>
                                    <td>{!! $question->negative !!}</td>
                                    <td>{!! $question->subject !!}</td>
                                    <td>{!! $question->category !!}</td>
                                    <td>{!! $question->level !!}</td>

                                </tr>
                                @endforeach</tbody>




                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

</div>
<!-- container -->
<script type="text/javascript">
$('#instructions').summernote();

</script>

@endsection
