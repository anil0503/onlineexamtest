@extends('layouts.vertical.master')
@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                                <div class="col-12">
                                   <div class="page-title-box">
                                      <div class="page-title-right">
                                            <a class="btn btn-success" href="{{ route('quizzes.create') }}"> Create New Quiz</a>

                                      </div>
                                      <h4 class="page-title">Quiz Managment</h4>

                            </div>
                                </div>
                             </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
    @if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif

                                        <table id="basic-datatable" class="table dt-responsive nowrap">
                                                <thead><tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>duration</th>
                                                        <th>start date</th>
                                                        <th>end date</th>
                                                        <th>Pass %</th>
                                                        <th>status</th>
                                                        <th width="280px">Action</th>
                                                    </tr>  </thead>

                                                    <tbody>
                                                            @foreach ($quizzes as $quiz)
                                                            <tr>
                                                        <td>{{ $quiz->id }}</td>

                                                        <td>{{ $quiz->name }}</td>
                                                        <td>{{ $quiz->duration }}</td>
                                                        <td>{{ $quiz->start_date }}</td>
                                                        <td>{{ $quiz->end_date }}</td>
                                                        <td>{{ $quiz->pass }}</td>
                                                        <td>{{ $quiz->status }}</td>
                                                        <td>
                                                            <form action="{{ route('quizzes.destroy',$quiz->id) }}" method="POST">

                                                                <a class="btn btn-info" href="{{ route('quizzes.show',$quiz->id) }}">View</a>

                                                                <a class="btn btn-primary" href="{{ route('quizzes.edit',$quiz->id) }}">Edit</a>

                                                                @csrf
                                                                @method('DELETE')

                                                                <button type="submit" class="btn btn-danger">Delete</button>
                                                            </form>
                                                        </td>
                                                    </tr> @endforeach</tbody>




                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->




                    </div> <!-- container -->
@endsection

@section('script')

        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection
