@extends('layouts.master-without-nav')

@section('content')

<div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div style="margin-top: -35px;" class="col-md-8 col-lg-6 col-xl-5">
                    <div  class="card">

                        <div  style="margin-top: -20px;" class="card-body p-4">

                            <div class="text-center w-75 m-auto">
                                <a href="{{ URL::asset('/')}}">
                                <br>
                                </a>
                                <p class="text-muted mb-4 mt-3">Don't have an account? Create your free account now.</p>
                            </div>

                            <h5  style="margin-top: -20px;" class="auth-title">Create Account</h5>


                            <form method="POST" action="{{ route('register') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name">Full Name</label>


                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                    </div>

                                    <div class="form-group row">
                                        <label for="email">E-Mail Address</label>


                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                    </div>
                                    <div class="form-group row">
                                            <label for="password">Password</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password">

                                        {{-- {!! Form::password('password', ['class' => 'form-control','placeholder' => '*Password']) !!} --}}
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                    {{-- <div class="form-group row">
                                        <label for="password">Password</label>


                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                    </div> --}}
                                    <div class="form-group">
                                            <div class="custom-control custom-checkbox checkbox-info">
                                                    <input type="checkbox" class="custom-control-input @error('checkbox-signup') is-invalid @enderror" id="checkbox-signup" name="checkbox-signup" value="1" />

                                                <label class="custom-control-label" id="checkbox-signup" name="checkterms" for="checkbox-signup">I accept <a href="javascript: void(0);" class="text-dark"><b>Terms and Conditions</b></a></label>
                                                @error('checkbox-signup')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            </div>
                                        </div>

                                    <input id="source" type="hidden" name="source" value="website" required autofocus>
                                    <input type="hidden" id="user_type" name="user_type" value="Super Admin">



                                    <div>
                                        <div >
                                            <button type="submit" class="btn btn-danger btn-block">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>


                                {{-- <div class="text-center">
                                    <h5 class="mt-3 text-muted">Sign in with</h5>
                                    <ul class="social-list list-inline mt-3 mb-0">

                                        <li class="list-inline-item">
                                            <a href="{{ url('/login/google/redirect') }}" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a href="{{ url('/login/facebook/redirect') }}" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                                        </li>
                                        {{-- <a href="{{ url('/redirect') }}" class="btn btn-primary">Login With Google</a> --}}

                                   {{--  </ul>
                                </div> --}}

                            </div> <!-- end card-body -->
                        </div>
                        <!-- end card -->

                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <p class="text-muted">Already have account?  <a href="{{ URL::asset('login')}}" class="text-muted ml-1"><b class="font-weight-semibold">Sign In</b></a></p>
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->

                    </div> <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end page -->

        <footer class="footer footer-alt">
            {{date('Y')}} &copy; Xeria theme by <a href="" class="text-muted">Coderthemes</a>
        </footer>

@endsection
