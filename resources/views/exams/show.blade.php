@extends('layouts.vertical.master')
@section('css')
<style>
   a, a:hover {
   color: white;
   }
   .btn-group > .btn, .btn-group-vertical > .btn {
   position: relative;
   flex: 1 1 auto;
   color: #6c757d;
   }
   .note-editor.note-frame .note-editing-area .note-editable {
   height: 200px;
   background-color: #f672a7;
   }
   /* .note-editor {
   height: 350px;
   position: relative;
   } */
</style>
@endsection
@section('content')
<link href="{{ URL::asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
{{--
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
--}}
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
<!-- Start Content-->
<div class="container-fluid">
   <!-- start page title -->
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <h4 class="page-title">Exam Name : {{$quizz->name}}</h4>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-xl-12">
         <div class="card-box">
            <div class="row">
               <div class="col-3">
                  <div class="form-group required">
                     {!! Form::label("startdate","Start Date",["class"=>"col-form-label"]) !!}
                     <input class="form-control" id="start_date" type="date" name="start_date" value="{{$quizz->start_date}}" readonly>
                  </div>
               </div>
               <div class="col-3">
                  <div class="form-group required">
                     {!! Form::label("enddate","End Date",["class"=>"col-form-label"]) !!}
                     <input class="form-control" id="end_date" type="date" name="end_date" value="{{$quizz->end_date}}" readonly>
                  </div>
               </div>
               <div class="col-3">
                  <div class="form-group required">
                     {!! Form::label("pass","Pass Percentage",["class"=>"col-form-label"]) !!}
                     <input class="form-control" id="pass" type="number" name="pass" value="{{$quizz->pass}}" readonly>
                  </div>
               </div>
               <div class="col-3">
                  <div class="form-group required">
                     {!! Form::label("duration","Duration",["class"=>"col-form-label"]) !!}
                     <input class="form-control" id="duration" type="number" name="duration" value="{{$quizz->duration}}" readonly>
                  </div>
               </div>
               <div class="col-12">
                  <div class="form-row">
                     {!!$quizz->instructions!!}
                     <div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- end card-box-->
   </div>
</div>
<!-- end page title -->
</div>
<!-- end row -->
<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body">
            <form action="{{ route('exams.store') }}" method="POST">
                    <input class="form-control" id="quiz_id"  type="hidden" name="quiz_id" value=" {!!$quizz->id!!}                    ">
                    <input class="form-control" id="user_id"  type="hidden" name="user_id" value="{{Auth::user()->id}}">

                @csrf
                <h4> Check the answer to each question, and click on the "Submit" button to submit the information.</h4>
               @foreach($questions as $question)
               @if($question->category == "MCQ")
               <div >
                  <table class="table table-bordered">
                     <tbody>
                        <tr class="text-info">
                        </tr>
                        <tr>
                           <td colspan="4">
                              <strong> Directions</strong>  {!! $question->context !!}
                              <strong>Question: </strong>{!! $question->question !!}
                           </td>
                        </tr>
                        <tr>
                           <td colspan="4">
                              <div class="mt-3">
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id=" {!! $question->id !!}1" name="ans_1" class="custom-control-input" value="1">
                                    <label class="custom-control-label" for=" {!! $question->id !!}1">{!! $question->option_1 !!}</label>
                                 </div>
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id=" {!! $question->id !!}2" name="ans_1" class="custom-control-input" value="2">
                                    <label class="custom-control-label" for=" {!! $question->id !!}2">{!! $question->option_2 !!}</label>
                                 </div>
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id=" {!! $question->id !!}3" name="ans_1" class="custom-control-input" value="3">
                                    <label class="custom-control-label" for=" {!! $question->id !!}3">{!! $question->option_3 !!}</label>
                                 </div>
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id=" {!! $question->id !!}4" name="ans_1" class="custom-control-input" value="4">
                                    <label class="custom-control-label" for=" {!! $question->id !!}4">{!! $question->option_4 !!}</label>
                                 </div>
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id=" {!! $question->id !!}5" name="ans_1" class="custom-control-input" value="5">
                                    <label class="custom-control-label" for=" {!! $question->id !!}5">{!! $question->option_5 !!}</label>
                                 </div>
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id=" {!! $question->id !!}6" name="ans_1" class="custom-control-input" value="6">
                                    <label class="custom-control-label" for=" {!! $question->id !!}6">{!! $question->option_6 !!}</label>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><strong>Marks :</strong> {!! $question->marks !!}</td>
                           <td><strong>Negative Marks :</strong> {!! $question->negative !!}</td>
                           <td><strong>Subject :</strong> {!! $question->subject !!}</td>
                           <td><strong>Dificulty Level :</strong> {!! $question->level !!}</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               @endif
               @if($question->category == "blanks")
               <div >
                  <table class="table table-bordered">
                     <tbody>
                        <tr class="text-info">
                        </tr>
                        <tr>
                           <td colspan="4">
                              <strong> Directions</strong>  {!! $question->context !!}
                              <strong>Question: </strong>{!! $question->question !!}
                           </td>
                        </tr>
                        <tr>
                           <td colspan="4">
                              <strong>Submit Answer </strong>
                              <input class="form-control" id="ans_2"  type="text" name="ans_2">
                           </td>
                        </tr>
                        <tr>
                           <td><strong>Marks :</strong> {!! $question->marks !!}</td>
                           <td><strong>Negative Marks :</strong> {!! $question->negative !!}</td>
                           <td><strong>Subject :</strong> {!! $question->subject !!}</td>
                           <td><strong>Dificulty Level :</strong> {!! $question->level !!}</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               @endif
               @if($question->category == "voice")
               <div >
                  <table class="table table-bordered">
                     <tbody>
                        <tr class="text-info">
                        </tr>
                        <tr>
                           <td colspan="4">
                              <strong> Directions</strong>  {!! $question->context !!}
                              <strong>Question: </strong>{!! $question->question !!}
                           </td>
                        </tr>
                        <tr>
                           <td colspan="4">
                              <strong>Submit Answer </strong>
                              <input class="form-control" id="ans_3"  type="text" name="ans_3">
                           </td>
                        </tr>
                        <tr>
                           <td><strong>Marks :</strong> {!! $question->marks !!}</td>
                           <td><strong>Negative Marks :</strong> {!! $question->negative !!}</td>
                           <td><strong>Subject :</strong> {!! $question->subject !!}</td>
                           <td><strong>Dificulty Level :</strong> {!! $question->level !!}</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
               @endif
               </p>
               @endforeach
               <input type="submit" class="btn btn-warning" value="Submit Answers">
               <input type="reset" class="btn btn-danger" value="Clear Answers">
            </form>
         </div>
         <!-- end card body-->
      </div>
      <!-- end card -->
   </div>
   <!-- end col-->
</div>
<!-- end row-->
</div>
@endsection
