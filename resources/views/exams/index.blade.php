@extends('layouts.vertical.master')
@section('css')
        <!-- third party css -->
        <link href="{{ URL::asset('assets/libs/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- third party css end -->
@endsection

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">

                        <!-- start page title -->
                        <div class="row">
                                <div class="col-12">
                                   <div class="page-title-box">

                                      <h4 class="page-title">All Exam List</h4>

                            </div>
                                </div>
                             </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">


                                        <table id="basic-datatable" class="table dt-responsive nowrap">
                                                <thead><tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>duration</th>
                                                        <th>start date</th>
                                                        <th>end date</th>
                                                        <th>Pass %</th>

                                                        <th width="280px">Action</th>
                                                    </tr>  </thead>

                                                    <tbody>
                                                            @foreach ($quizzes as $quiz)
                                                            <tr>
                                                        <td>{{ $quiz->id }}</td>

                                                        <td>{{ $quiz->name }}</td>
                                                        <td>{{ $quiz->duration }}</td>
                                                        <td>{{ $quiz->start_date }}</td>
                                                        <td>{{ $quiz->end_date }}</td>
                                                        <td>{{ $quiz->pass }}</td>

                                                        <td>

                                                                <a class="btn btn-info" href="{{ route('exams.show',$quiz->id) }}">Start Exam</a>

                                                        </td>
                                                    </tr> @endforeach</tbody>




                                        </table>

                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        <!-- end row-->




                    </div> <!-- container -->
@endsection

@section('script')

        <!-- third party js -->
        <script src="{{ URL::asset('assets/libs/datatables/datatables.min.js')}}"></script>
        <script src="{{ URL::asset('assets/libs/pdfmake/pdfmake.min.js')}}"></script>
        <!-- third party js ends -->

        <!-- Datatables init -->
        <script src="{{ URL::asset('assets/js/pages/datatables.init.js')}}"></script>

@endsection
