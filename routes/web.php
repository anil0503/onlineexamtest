<?php
Route::get('user/login/', 'CustomersController@login');

Route::get('user/register/', 'CustomersController@register');

Route::resource('users', 'UserController')->middleware('Admin');
Route::resource('questions', 'QuestionsController')->middleware('Admin');
Route::resource('quizzes', 'QuizzesController')->middleware('Admin');
Route::resource('exams', 'ExamController')->middleware('customer');
Route::resource('addquestions', 'AddQuestionsController')->middleware('Admin');
Route::resource('results', 'ResultsController')->middleware('customer');
Route::resource('adminresults', 'AdminResultsController')->middleware('Admin');





// Route::group(['middleware' => ['auth']], function () {
//     Route::resource('roles', 'RoleController');
//     Route::resource('users', 'UserController');
//     Route::resource('products', 'ProductController');
// });

Route::get('/user/dashboard', 'CustomersController@dashboard')->name('userposts');
Route::post('/user/dashboard', 'CustomersController@dashboard')->name('userposts');

Route::get('/dashboard', 'DashboardController@index')->name('getposts');


//Route::resource('account', 'ProfileController');
Route::post('/account/update/{id}', 'ProfileController@updateuser')->name('account.updateuser');

//Route::get('account/{id}', ['as' => 'account.edit', 'uses' => 'UserController@edit']);
Route::patch('account/{id}/update', ['as' => 'account.updateuser', 'uses' => 'ProfileController@updateuser']);

Route::get('/', function () {
    return redirect('dashboard');
});
Route::get('/lockscreen', function () {
    return view('accounts.lockscreen');
});
Route::get('/posts', function () {
    return redirect('dashboard');
});
Route::get('/singleboard', function () {
    return view('singleboard');
});
Route::get('/account', 'ProfileController@index');
Route::get('/team', function () {
    return view('team');
});
Route::get('/sendemail', 'SendEmailController@index');
Route::post('/sendinviteemails', 'SendEmailController@send')->name('sendemail.email');
Route::resource('betafeedback', 'MailController');

Route::get('404', ['as' => '404', 'uses' => 'ErrorHandlerController@errorCode404']);
//Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);
Auth::routes(['verify' => true]);
// Route::get('/', 'BrandController@create');
Route::get('/verify', 'HomeController@verify');
//Route::get('/brands/index', 'BrandController@index');
