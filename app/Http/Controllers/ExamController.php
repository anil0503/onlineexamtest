<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Quiz;
use App\Question;
use App\Result;
use App\AddQuestions;
use Illuminate\Http\Request;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {

        $quizzes = Quiz::orderBy('created_at', 'desc')->get();
        return view('exams.index', compact('quizzes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'ans_1' => 'required',
            // '' => 'required',
            // 'instructions' => 'required',
            // 'start_date' => 'required',
            // 'end_date' => 'required',
            // 'pass' => 'required',
            // 'status' => 'required',
        ]);

        //Quiz::create($request->all());
        Result::create([

            'ans_1' => $request['ans_1'],
            'ans_2' => $request['ans_2'],
            'ans_3' => $request['ans_3'],
            'ans_4' => $request['ans_4'],
            'ans_5' => $request['ans_5'],
            'ans_6' => $request['ans_6'],
            'quiz_id' => $request['quiz_id'],
            'user_id' => $request['user_id'],


        ]);
        return redirect()->route('exams.index')
            ->with('success', 'Answers Submitted  successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quizz = Quiz::findOrFail($id);
        $questions = Question::join('quiz_questions', 'exam_question.id', '=', 'quiz_questions.question_id')
            ->selectRaw('exam_question.*')
            ->get();
        $data = array(
            'quizz' => $quizz,
            'questions' => $questions,
        );
        return view('exams.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz)
    {
        return view('exams.edit', compact('quiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        $request->validate([
            'name' => 'required',
            'duration' => 'required',
            'instructions' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'pass' => 'required',
            'status' => 'required',
        ]);

        $quiz->update($request->all());

        return redirect()->route('quizzes.index')
            ->with('success', 'Exam updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quizzes)
    {
        $quizzes->delete();

        return redirect()->route('quizzes.index')
            ->with('success', 'Exam deleted successfully');
    }
}
