<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Brand;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands =  Brand::where('user_id', '=', Auth::user()->id)->get();

        $data = array(
            'brands' => $brands,

        );
        return view('account')->with($data);
        // $users = User::all();
        // return view('account', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show', compact('post'));
    }
    public function edit($id)
    {
        $task = Post::findOrFail($id);

        return view('posts.edit')->withTask($task);
    }

    public function updateuser(User $user)
    {
        if (Auth::user()->email == request('email')) {

            $this->validate(request(), [
                'name' => 'required',
                //  'email' => 'required|email|unique:users',
                'password' => 'required|min:6|confirmed'
            ]);

            $user->name = request('name');
            // $user->email = request('email');
            $user->password = bcrypt(request('password'));

            $user->save();

            return back();
        } else {

            $this->validate(request(), [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6|confirmed'
            ]);

            $user->name = request('name');
            $user->email = request('email');
            $user->password = bcrypt(request('password'));

            $user->save();

            return back();
        }
    }
    public function updatebrand($id, Request $request)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);
        $input = $request->all();
        $user->fill($input)->save();
        Session::flash('flash_message', 'post successfully added!');
        return redirect('/account');
    }
}
