<?php

namespace App\Http\Controllers;

use App\Post;
use App\Brand;
use App\Board;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use DB;
use Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('verified');
    }

    public function index(Request $request)
    {

        return view('dashboard');
    }
}
