<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Quiz;
use App\Question;

use Auth;
use App\Result;
use App\AddQuestions;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {

        // $quizzes = Result::join('quizzes', 'quizzes.id', '=', 'quiz_id')
        //     ->join('users', 'users.id', '=', 'user_id')
        //     ->get();

        $quizzes = Result::join('quizzes', 'quizzes.id', '=', 'quiz_id')
            ->where('results.user_id',  '=', Auth::user()->id)
            //  ->join('quiz_questions', 'quiz_questions.exam_id', '=', 'exam_id')
            //->join('exam_question', 'exam_question.id', '=', 'quiz_questions.question_id')
            ->join('users', 'users.id', '=', 'results.user_id')
            ->get();

        $results = AddQuestions::join('exam_question', 'exam_question.id', '=', 'question_id')
            ->join('quizzes', 'quizzes.id', '=', 'quiz_questions.exam_id')
            ->join('results', 'results.quiz_id', '=', 'quizzes.id')
            //->join('users', 'users.id', '=', 'results.user_id')
            ->get();

        // $results = Result::join('quizzes', 'quizzes.id', '=', 'quiz_id')
        //     ->join('quiz_questions', 'quiz_questions.exam_id', '=', 'exam_id')
        //     ->join('exam_question', 'exam_question.id', '=', 'quiz_questions.question_id')
        //     //->join('users', 'users.id', '=', 'results.user_id')
        //     ->get();
        $data = array(
            'quizzes' => $quizzes,
            'results' => $results,

        );
        return view('results.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('results.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'duration' => 'required',
            'instructions' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'pass' => 'required',
            'status' => 'required',
        ]);

        //Quiz::create($request->all());
        Quiz::create([

            'name' => $request['name'],
            'duration' => $request['duration'],
            'instructions' => $request['instructions'],
            'start_date' => $request['start_date'],
            'end_date' => $request['end_date'],
            'status' => $request['status'],
            'pass' => $request['pass'],


        ]);
        return redirect()->route('quizzes.index')
            ->with('success', 'Result created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        // $quizzes = Result::join('quizzes', 'quizzes.id', '=', 'quiz_id')
        //     ->join('users', 'users.id', '=', 'user_id')
        //     ->get();

        $quizzes = Result::join('quizzes', 'quizzes.id', '=', 'quiz_id')
            //  ->join('quiz_questions', 'quiz_questions.exam_id', '=', 'exam_id')
            //->join('exam_question', 'exam_question.id', '=', 'quiz_questions.question_id')
            ->join('users', 'users.id', '=', 'results.user_id')
            ->get();

        $results = AddQuestions::join('exam_question', 'exam_question.id', '=', 'question_id')
            ->join('quizzes', 'quizzes.id', '=', 'quiz_questions.exam_id')
            ->join('results', 'results.quiz_id', '=', 'quizzes.id')
            ->join('users', 'users.id', '=', 'results.user_id')
            ->get();

        // $results = Result::join('quizzes', 'quizzes.id', '=', 'quiz_id')
        //     ->join('quiz_questions', 'quiz_questions.exam_id', '=', 'exam_id')
        //     ->join('exam_question', 'exam_question.id', '=', 'quiz_questions.question_id')
        //     //->join('users', 'users.id', '=', 'results.user_id')
        //     ->get();
        $data = array(
            'quizzes' => $quizzes,
            'results' => $results,

        );
        return view('results.show')->with($data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz)
    {
        return view('results.edit', compact('quiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        $request->validate([
            'name' => 'required',
            'duration' => 'required',
            'instructions' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'pass' => 'required',
            'status' => 'required',
        ]);

        $quiz->update($request->all());

        return redirect()->route('quizzes.index')
            ->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quizzes)
    {
        $quizzes->delete();

        return redirect()->route('quizzes.index')
            ->with('success', 'Product deleted successfully');
    }
}
