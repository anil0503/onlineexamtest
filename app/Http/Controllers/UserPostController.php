<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\Brand;
use App\Board;
use App\Comment;
use DB;
use Auth;


class UserPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {






        $brands =  Brand::where('user_id', '=', Auth::user()->id)->get();
        $boards =  Board::where('user_id', '=', Auth::user()->id)->get();
        $posts =  Post::where('board_id', '=', $boards[0]->id)->paginate(3);

        $status = DB::table('posts')
            ->join('boards', 'boards.id', '=', 'posts.board_id')
            ->select('boards.pri_status1', 'boards.pri_status2', 'boards.pri_status3')
            ->get();

        $data = array(
            'brands' => $brands,
            'boards' => $boards,
            'posts' => $posts,
            'status' => $status,
        );
        return view('customers.posts.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('customers.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => ['required', 'string', 'min:5', 'max:255'],
            'body' => ['required', 'string', 'min:5', 'max:255'],
        ]);
        $input = new Post;
        $input->title = $request->input('title');
        $input->body = $request->input('body');
        //$input->status = $request->input('pstatus');
        $input->priority = $request->input('regencies');
        $input->board_id = $request->input('provinces');
        $input->brand_id = $request->get('Brand');
        $input->user_id = auth()->user()->id;

        $input->save();

        return redirect()->route('customers.posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $pieces = explode('.', $request->getHost());
        // Remove the subdomain param from being passed any further
        $request->route()->forgetParameter('subdomain');
        // If the subdomain is not part of the current
        // companies array then kick the user to app.winkhq.com
        $subdomains = Brand::pluck('subdomain')->all();
        if (!in_array($pieces[0], $subdomains)) {
            return redirect()->to("http://tribevote.com");
        }
        // The subdomain is currently setup
        $request->session()->put('subdomain', $pieces[0]);
        $domain = $pieces[0];


        $subdomain = DB::table('brands')->select('id')
            ->where('subdomain', '=', $domain)
            ->orderBy('id', 'asc')
            ->value('id');

        $brands =  Brand::where('user_id', '=', $subdomain)->get();
        $post = Post::find($id);
        $boards =  DB::table('boards')->where('boards.user_id', '=', $subdomain)
            ->get();
        $status = DB::table('posts')
            ->where('posts.id', '=', $id)
            ->rightJoin('boards', 'boards.id', '=', 'posts.board_id')
            ->select('boards.pri_status1', 'boards.pri_status2', 'boards.pri_status3')
            ->get();

        $data = array(
            'brands' => $brands,
            'boards' => $boards,
            'post' => $post,
            'status' => $status,
        );
        return view('customers.posts.show')->with($data);

        // $brands =  Brand::where('user_id', '=', Auth::user()->id)->get();
        // $boards =  Board::where('user_id', '=', Auth::user()->id)->get();
        // // //$posts = Post::find($id);
        // $post = Post::find($id);
        // // ->leftJoin('comment', 'posts.id', '=', 'comment.post_id')
        // // ->selectRaw('posts.*, count(comment.post_id) as commentcount')
        // // ->groupBy('posts.id')
        // // ->get();
        // $data = array(
        //     'brands' => $brands,
        //     'boards' => $boards,
        //     'post' => $post,
        // );
        // //return view('posts.show', compact('data'));

        // return view('posts.show')->with($data);
    }
    public function edit($id)
    {

        $task = Post::findOrFail($id);

        return view('customers.posts.edit')->withTask($task);
    }

    public function dashboard()
    {


        //  $posts = [];

        //  if ( !empty ( $boardsarray ) ) {

        //     $posts =  Post::whereIn('board_id', $boardsarray)->get();
        //   }
        //   else
        //   {
        // $posts =  Post::where('board_id','=',Auth::user()->id)->get();
        $boards =  Board::where('user_id', '=', Auth::user()->id)->get();
        $posts =  Post::where('board_id', '=', $boards[0]->id)->get();
        //}
        $data = array(
            'boards' => $boards,
            'posts' => $posts,
        );
        return view('home')->with($data);

        // return view('dashboard')->with('infos', 'demo');

    }
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('user/posts');
    }
    public function update($id, Request $request)
    {
        $task = Post::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        $input = $request->all();

        $task->fill($input)->save();

        //Session::flash('flash_message', 'post successfully added!');

        return redirect('user/posts');
    }











    //     public function index1()
    //     {
    //          $boards =  Board::where('user_id','=',Auth::user()->id)->get();
    //         $data = array(
    //             'boards'=> $boards ,
    //             );
    //             $selectedID = 2;
    //         return view('home')->with($data);

    //         // return view('dashboard')->with('infos', 'demo');

    //     }



}






//Backup  Code
// public function __construct()
// {
//     $this->middleware('auth');
// }
// public function index()
// {
// $infos = DB::table('posts')
// ->where('user_id', '=', Auth::user()->id)
// ->first();

// if (is_null($infos)) {

// //return 'anil';
// return view('brands.create-step1')->with('infos', $infos);
// } else {
// return redirect('posts/create')->with('infos', $infos);
// }
// }

// public function create()
// {
// return view('posts.create');
// }
// public function store(Request $request)
// {

// $input = new Post;
// $input->title = $request->input('title');
// $input->body = $request->input('body');
// $input->status = $request->input('pstatus');
// $input->priority = $request->input('priority');
// $input->brand_id = $request->get('Brand');
// $input->user_id = auth()->user()->id;

// $input->save();


// return redirect('/');
// //Post::create($request->all());
// }
// public function show($id)
// {
// $task = Post::findOrFail($id);

// return view('tasks.show')->withTask($task);
// }
// public function edit($id)
// {
// $task = Post::findOrFail($id);

// return view('posts.edit')->withTask($task);
// }
