<?php

namespace App\Http\Controllers;

use App\Post;
use App\Brand;
use App\Board;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;
use Illuminate\View\View;

use DB;
use Auth;


class CustomersController extends Controller
{



    public function login(Request $request)
    {

        return view('customers.auth.login');
    }
    public function register(Request $request)
    {
        return view('customers.auth.register');
    }

    public function environment(Request $request)
    {
        return view('environment');
    }



    public function domain(Request $request)
    {
        echo 'start <br>';
        $domainitem = $request->input('domain');


        return view('customers.domain');
    }


    public function dashboard(Request $request)
    {
        return view('customers.dashboard');
    }
    function fetch(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $dependent = $request->get('dependent');
        $data = DB::table('boards')
            ->where($select, $value)
            ->groupBy($dependent)
            ->get();
        $output = '<option value="">Select ' . ucfirst($dependent) . '</option>';
        foreach ($data as $row) {
            $output .= '<option value="' . $row->$dependent . '">' . $row->$dependent . '</option>';
        }
        echo $output;
    }
    public function provinces()
    {
        $provinces = Board::all();
        return view('user/dashboard', compact('provinces'));
    }

    public function regencies()
    {
        $provinces_id = Input::get('province_id');
        $regencies = Board::where('id', '=', $provinces_id)->get();
        echo $regencies;
        return response()->json($regencies);
    }
}
