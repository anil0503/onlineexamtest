<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Post;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $brands = DB::table('brands')->where('user_id', Auth::user()->id)->first();

        if (!empty($brands)) {
            $boards = DB::table('boards')->where('user_id', Auth::user()->id)->first();
            if (!empty($boards)) {
                $posts = DB::table('posts')->where('user_id', Auth::user()->id)->first();
                if (!empty($posts)) {
                    if (Auth::user()->send_invites == '1') {
                        return view('/');
                    } else {
                        return redirect('/step4');
                    }
                } else {
                    return redirect('/step3');
                }
            } else {
                return redirect('/step2');
            }
        } else {
            return redirect('/step1');
        }
    }
    public function verify()
    {
        return view('auth.verify');
    }


    public function ajaxRequest(Request $request)
    {
        $post = Post::find($request->id);
        $response = auth()->user()->toggleLike($post);
        return response()->json(['success' => $response]);
    }
    public function users()
    {
        $users = User::all();
        return view('users', compact('users'));
    }
}
