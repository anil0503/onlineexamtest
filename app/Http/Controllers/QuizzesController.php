<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Quiz;
use App\Question;
use App\AddQuestions;
use Illuminate\Http\Request;

class QuizzesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {

        $quizzes = Quiz::orderBy('created_at', 'desc')->get();
        return view('quizzes.index', compact('quizzes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('quizzes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'duration' => 'required',
            'instructions' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'pass' => 'required',
            'status' => 'required',
        ]);

        //Quiz::create($request->all());
        Quiz::create([

            'name' => $request['name'],
            'duration' => $request['duration'],
            'instructions' => $request['instructions'],
            'start_date' => $request['start_date'],
            'end_date' => $request['end_date'],
            'status' => $request['status'],
            'pass' => $request['pass'],


        ]);
        return redirect()->route('quizzes.index')
            ->with('success', 'Quiz created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz)
    {
        // $questions = Question::orderBy('created_at', 'desc')->get();
        $quizzes = Quiz::orderBy('created_at', 'desc')->get();
        // $addquestions = AddQuestions::rightJoin('exam_question', 'exam_question.id', '=', 'quiz_questions.question_id')
        //     ->selectRaw('quiz_questions.question_id')
        //     ->first();
        $questions = Question::join('quiz_questions', 'exam_question.id', '=', 'quiz_questions.question_id')
            ->selectRaw('exam_question.*')
            ->get();
        $data = array(
            'quiz' => $quiz,
            'quizzes' => $quizzes,
            'questions' => $questions,
            // 'addquestions' => $addquestions,

        );
        return view('quizzes.show')->with($data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Quiz $quiz)
    {
        return view('quizzes.edit', compact('quiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz)
    {
        $request->validate([
            'name' => 'required',
            'duration' => 'required',
            'instructions' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'pass' => 'required',
            'status' => 'required',
        ]);

        $quiz->update($request->all());

        return redirect()->route('quizzes.index')
            ->with('success', 'Quiz updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quizzes)
    {
        $quizzes->delete();

        return redirect()->route('quizzes.index')
            ->with('success', 'Quiz deleted successfully');
    }
}
