<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Question;
use App\Quiz;
use App\AddQuestions;
use Illuminate\Http\Request;
use App\User;

class AddQuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $quiz = Quiz::orderBy('created_at', 'desc')->get();
        //$addquestions = AddQuestions::all();
        //$questions = Question::orderBy('created_at', 'desc')->get();

        // $questions = Question::rightJoin('quiz_questions', 'quiz_questions.question_id', '=', 'c.id')
        //     //  ->selectRaw('exam_question.*')

        //     ->get();
        $questions = Question::join('quiz_questions', 'exam_question.id', '=', 'quiz_questions.question_id')
            ->selectRaw('exam_question.*')
            ->get();
        $questionsadd = Question::leftJoin('quiz_questions', 'exam_question.id', '=', 'quiz_questions.question_id')
            ->selectRaw('exam_question.*')
            ->get();
        $data = array(
            'quiz' => $quiz,
            // 'key' => $key,
            'questions' => $questions,
            'questionsadd' => $questionsadd,
        );
        return view('addquestions.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addquestions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'question_id' => 'required',
            'exam_id' => 'required',

        ]);
        $question = array(
            'question_id' => $request->question_id,
            'exam_id' => $request->exam_id,
        );
        AddQuestions::create($question);
        return redirect()->back()
            ->with('success', 'Question added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('addquestions.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        return view('addquestions.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $request->validate([
            'question' => 'required',
            'answers' => 'required',
            'marks' => 'required',
            'negative' => 'required',
            'context' => 'required',
            'level' => 'required',
            'category' => 'required',
            'subject' => 'required',
        ]);

        $question->update($request->all());

        return redirect()->route('questions.index')
            ->with('success', 'Question updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(AddQuestions $addquestion)
    {
        $addquestion->delete();

        return redirect()->back()
            ->with('success', 'Question deleted successfully');
    }
}
