<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

use App\Http\Middleware\Route;
use DB;
use Closure;
use App\Brand;
use Auth;
use App\Board;
use App\Post;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        return $next($request);
    }
}
