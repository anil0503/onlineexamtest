<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_type == "Customer") {
            return $next($request);
        } else {
            return redirect('dashboard');
        }
    }
}
