<?php

namespace App;

use SoftDeletes;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $fillable = [
        'id', 'question', 'option_1', 'option_2', 'option_3', 'option_4', 'option_5', 'option_6', 'category', 'level', 'subject', 'created_by', 'answers', 'marks', 'negative', 'context',
    ];

    protected $table = 'exam_question';


    protected $dates = ['deleted_at'];
}
