<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verify extends Model
{
    protected $fillable = ['email', 'user_id'];
    protected $casts = [
        'email' => 'array',
      ];
}
