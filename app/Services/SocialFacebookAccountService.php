<?php

namespace App\Services;

use App\SocialFacebookAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user                  = new User;
                $user->name            = $providerUser->getName();
                $user->email           = $providerUser->getEmail();
                $user->google_id       = $providerUser->getId();
                $user->avatar          = $providerUser->getAvatar();
                $user->avatar_original = $providerUser->getAvatar();
                $user->user_type       = 'Super Admin';
                $user->save();
                auth()->login($user, true);
                // $user = User::create([
                //     $user->email => $providerUser->getEmail(),
                //     $user->name => $providerUser->getName(),
                //     $user->password => md5(rand(1, 10000)),
                //     $user->avatar_original => $providerUser->getAvatar(),
                //     $user->user_type => "Super Admin",
                // ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}
