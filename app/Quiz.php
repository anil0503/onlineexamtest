<?php

namespace App;

use SoftDeletes;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{

    protected $fillable = [
        'id', 'name', 'duration', 'instructions', 'start_date', 'end_date', 'pass', 'status',
    ];

    protected $table = 'quizzes';


    protected $dates = ['deleted_at'];
}
