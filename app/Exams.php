<?php

namespace App;

use SoftDeletes;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Illuminate\Database\Eloquent\Model;

class Exams extends Model
{
    protected $fillable = [
        'id', 'exam_id', 'user_id', 'ans_1', 'ans_2', 'ans_3', 'ans_4', 'ans_5', 'ans_6',
    ];

    protected $table = 'exams';

    protected $dates = ['deleted_at'];
}
