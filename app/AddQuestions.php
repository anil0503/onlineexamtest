<?php

namespace App;

use SoftDeletes;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Illuminate\Database\Eloquent\Model;

class AddQuestions extends Model
{
    protected $fillable = [
        'id', 'question_id', 'exam_id',
    ];

    protected $table = 'quiz_questions';

    protected $dates = ['deleted_at'];
}
