<?php

namespace App;

use SoftDeletes;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{

    protected $fillable = [
        'id', 'ans_1', 'ans_2', 'ans_3', 'ans_4', 'ans_5', 'ans_6', 'quiz_id', 'user_id'
    ];

    protected $table = 'results';


    protected $dates = ['deleted_at'];
}
